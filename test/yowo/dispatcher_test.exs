defmodule Yowo.DispatcherTest do
	use ExUnit.Case, async: true

	@tag :distributed
	test "starts task across nodes" do
		# have each node reply w/ node name
		assert Yowo.Dispatcher.async(0, Kernel, :node, [])
			|> Task.await() == :bar@sager
		assert Yowo.Dispatcher.async(1, Kernel, :node, [])
			|> Task.await() == :foo@sager
	end

	@tag :distributed
	test "broadcasts to all nodes" do
		name = :test_dispatch
		Process.register(self(), name)
		Yowo.Dispatcher.multi_async(Yowo.Dispatcher, :report_hello, [{name, node()}])
		assert_receive "Hello from foo@sager"
		assert_receive "Hello from bar@sager"
		Process.unregister(name)
	end
end