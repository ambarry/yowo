defmodule Yowo.UserTest do
  use ExUnit.Case, async: false
  alias :mnesia, as: Mnesia
  alias Yowo.User
  alias Yowo.User.Database, as: UserDb
  alias Yowo.TotalRecall, as: Recall


  @user_opts Application.get_env(:yowo, :user)
  @user_tab @user_opts[:user_table]
  @auth_tab @user_opts[:auth_table]
  @follows_tab @user_opts[:follows_table]
  @blocks_tab @user_opts[:blocks_table]
  

  setup_all do
    :ok = Mnesia.start()
    
    UserDb.init_schema() # NOTE: you'll have issues if schema not created with create_schema([node()])
    # try to start GenServer in case test is run with --no-start
    # otherwise, already_started, no worries
    tables = UserDb.tables()
    tables |> Mnesia.wait_for_tables(10_000) # NOTE: make sure you wait to access tables!
    tables
    |> Enum.each(fn tab -> 
      fn -> Mnesia.clear_table(tab) end
      |> Recall.sync_act()
    end)
    User.get_server_name(0)
    |> User.start_link()
    :ok
  end


  defp clear_tables() do
    UserDb.tables()
    |> Enum.each(fn t -> 
        {:atomic, :ok} = fn -> Mnesia.clear_table(t) end
        |> Recall.sync_act()
    end)
  end

  setup context do

    clear_tables()

    # preload users if specified by test
    if context[:load_users] do
      user = %User{:user_id => "id1", :username => "c0c0", :email => "c0c0@yowo.com"}
      user_2 = %User{:user_id => "id2", :username => "test_user", :email => "testuser@yowo.com"}
      user_3 = %User{:user_id => "id3", :username => "someguy", :email => "someguy@yowo.com"}

      # these need to use proper access module...
      fn -> Mnesia.write({@user_tab, user.user_id, user}) end
      |> Recall.sync_act()
      fn -> Mnesia.write({@user_tab, user_2.user_id, user_2}) end
      |> Recall.sync_act()
      fn -> Mnesia.write({@user_tab, user_3.user_id, user_3}) end
      |> Recall.sync_act()
      fn -> Mnesia.write({@auth_tab, user.username, user.email, user.user_id, nil, nil}) end
      |> Recall.sync_act()
      fn -> Mnesia.write({@auth_tab, user_2.username, user_2.email, user_2.user_id, nil, nil}) end
      |> Recall.sync_act()
      fn -> Mnesia.write({@auth_tab, user_3.username, user_3.email, user_3.user_id, nil, nil}) end
      |> Recall.sync_act()

      {:ok, %{users: [user, user_2, user_3]}}
    else
      :ok
    end
  end

  test "adds new user to db" do
    assert [] == fn -> Mnesia.all_keys(@user_tab) end |> Recall.sync_act()
    {:ok, user} = User.create("coconaut", "coconaut@thisisnotrealemail.com", "Password1234")
    :timer.sleep(200)
    id = user.user_id
    assert [^id] = fn -> Mnesia.all_keys(@user_tab) end |> Recall.sync_act()
    assert [{@user_tab, ^id, ^user}] = fn -> Mnesia.read(@user_tab, id) end |> Recall.sync_act()
  end

  test "finds valid user on lookup" do
    {:ok, user} = User.create("coconaut", "coconaut@thisisnotrealemail.com","Password1234")
    :timer.sleep(200)
    assert {:ok, ^user} = User.lookup(user.user_id)
  end

  test "returns error on lookup bad id" do
    assert :error = User.lookup("thisisnotarealuserid")
  end

  test "returns error on duplicate username" do
    name = "coconaut"
    {:ok, _user} = User.create(name, "test1@test.com", "Password1234")
    :timer.sleep(200)
    assert {:error, :username_taken} = User.create(name, "test2@test.com", "Password1234")
  end

  test "returns error on duplicate email" do
    email = "coconaut@fakeemail.com"
    {:ok, _user} = User.create("coconaut1", email, "Password1234")
    :timer.sleep(200)
    assert {:error, :email_taken} = User.create("coconaut2", email, "Password1234")
  end

  test "returns error on duplicate username and email" do
    name = "coconaut"
    email = "coconaut@notarealemail.org"
    {:ok, _user} = User.create(name, email, "Password1234")
    :timer.sleep(200)
    assert {:error, :username_and_email_taken} = User.create(name, email, "Password1234")
  end

  test "retrieves valid user on init" do
    assert [] = Mnesia.dirty_all_keys(@user_tab)
    {:ok, user} = User.create("coconaut", "test@cocococo.com", "Password1234")
    :timer.sleep(200)
    assert {:ok, ^user} = User.init_user(user.user_id)
  end

  test "returns error on init bad id" do
    {:ok, _user} = User.create("coconaut", "test@cocococo.com", "Password1234")
    :timer.sleep(200)
    assert {:error, _r} = User.init_user("thisisnotarealid")
  end

  @tag :load_users
  test "adds hap to user", %{:users => [user, _user_2, _user_3]} do
    assert %{} = user.haps
    hap = %Yowo.Hap{:date_expires => Yowo.DateTime.now() + 10_000}
    assert {:ok, u} = User.put_hap(user, hap)

    :timer.sleep(200)

    assert Map.has_key?(u.haps, hap.hap_id)
    assert u.haps[hap.hap_id] == hap.date_expires
    {:ok, db_user} = User.lookup(user.user_id)
    assert Map.has_key?(db_user.haps, hap.hap_id)
  end

  @tag :load_users
  test "adds multiple haps to user", %{:users => [user, _user_2, _user_3]} do
    assert %{} = user.haps
    hap_1 = %Yowo.Hap{}
    hap_2 = %Yowo.Hap{}
    {:ok, u} = User.put_haps(user, [hap_1, hap_2])

    :timer.sleep(100)

    assert Map.has_key?(u.haps, hap_1.hap_id)
    assert Map.has_key?(u.haps, hap_2.hap_id)
    {:ok, db_user} = User.lookup(user.user_id)
    assert Map.has_key?(db_user.haps, hap_1.hap_id)
    assert Map.has_key?(db_user.haps, hap_2.hap_id)
  end

  @tag :load_users
  test "removes expired haps on init", %{:users => [user, _user_2, _user_3]} do
    hap = %{:hap_id => :a, :date_expires => Yowo.DateTime.now() - 1000}
    hap_2 = %{:hap_id => :b, :date_expires => Yowo.DateTime.now() + 100000}
    hap_3 = %{:hap_id => :c, :date_expires => Yowo.DateTime.now() + 200000}
    hap_4 = %{:hap_id => :d, :date_expires => Yowo.DateTime.now() - 200000}

    map =
      [hap, hap_2, hap_3, hap_4]
      |> Enum.reduce(Map.new(), fn h, map -> Map.put(map, h.hap_id, h.date_expires) end)

    fn ->
      u = %{user | :haps => map}
      {@user_tab, u.user_id, u}
      |> Mnesia.write()
    end
    |> Mnesia.transaction()
    
    {:ok, inited} = User.init_user(user.user_id)
    assert (inited.haps |> Enum.count()) == 2
    assert Map.has_key?(inited.haps, :b)
    assert Map.has_key?(inited.haps, :c)
    refute Map.has_key?(inited.haps, :a)
    refute Map.has_key?(inited.haps, :d)
  end

  @tag :load_users
  test "doesn't add duplicate hap", %{:users => [user, _user_2, _user_3]} do
    hap = %Yowo.Hap{:hap_id => :a, :date_expires => Yowo.DateTime.now() + 10000}
    assert {:ok, u} = User.put_hap(user, hap)

    :timer.sleep(100)

    assert :error = User.put_hap(u, hap)
    assert {:ok, u} = User.lookup(user.user_id)
    assert Enum.count(u.haps) == 1
  end

  @tag :load_users
  test "adds new location to user", %{:users => [user, _user_2, _user_3]} do
    assert MapSet.new() == user.locations
    loc = {40.4, 70.8}
    assert {:ok, u} = User.put_location(user, loc)
    :timer.sleep(100)
    assert Enum.count(u.locations) == 1
    assert loc in u.locations
    {:ok, user} = User.lookup(user.user_id)
    assert Enum.count(u.locations) == 1
    assert loc in user.locations
  end

  @tag :load_users
  test "removes location from user", %{:users => [user, _user_2, _user_3]} do
    assert MapSet.new() == user.locations
    loc = {40.8, 70.4}
    loc_2 = {50.5, 50.5}
    assert {:ok, u} = User.put_location(user, loc)
    :timer.sleep(100)
    assert {:ok, u} = User.put_location(u, loc_2)
    :timer.sleep(100)
    assert Enum.count(u.locations) == 2
    {:ok, user} = User.remove_location(u, loc)
    :timer.sleep(100)
    assert Enum.count(user.locations) == 1
    assert loc_2 in user.locations
    {:ok, user} = User.lookup(user.user_id)
    assert Enum.count(user.locations) == 1
    assert loc_2 in user.locations
  end

  @tag :load_users
  test "doesn't add duplicate location", %{:users => [user, _user_2, _user_3]} do
    assert Enum.count(user.locations) == 0
    assert MapSet.new() == user.locations
    loc = {40.8, 70.4}
    assert {:ok, user} = User.put_location(user, loc)

    :timer.sleep(100)
    
    assert :error = User.put_location(user, loc)

    :timer.sleep(100)

    assert {:ok, user} = User.lookup(user.user_id)
    assert Enum.count(user.locations) == 1
    assert loc in user.locations
  end

  @tag :load_users
  test "adds follow to user", %{:users => [user, user_2, _user_3]} do
    id1 = user.user_id
    {id2, un2} = {user_2.user_id, user_2.username}
    assert user.follows == Map.new()
    assert [] == Mnesia.dirty_all_keys(@follows_tab)
    assert {:ok, u} = User.follow(user, id2, un2)
    :timer.sleep(100)
    assert Enum.count(u.follows) == 1
    assert Map.has_key?(u.follows, id2)
    {:ok, u} = User.init_user(id1) # NOTE: only init is guaranteed to retrieve new locs
    assert Enum.count(u.follows) == 1
    assert Map.has_key?(u.follows, id2)
    assert [{@follows_tab, ^id1, ^id2, ^un2}] = Mnesia.dirty_read(@follows_tab, id1)
  end

  @tag :load_users
  test "removes follow", %{:users => [user, user_2, _user_3]} do
    id1 = user.user_id
    {id2, un2} = {user_2.user_id, user_2.username}
    assert {:ok, u} = User.follow(user, id2, un2)
    :timer.sleep(100)
    assert [{@follows_tab, ^id1, ^id2, ^un2}] = Mnesia.dirty_read(@follows_tab, id1)
    assert {:ok, u} = User.unfollow(u, id2, un2)
    :timer.sleep(100)
    assert Enum.count(u.follows) == 0
    {:ok, u} = User.init_user(id1)
    assert Enum.count(u.follows) == 0
    assert [] == Mnesia.dirty_read(@follows_tab, id1)
  end

  @tag :load_users
  test "doesn't add duplicate follow", %{:users => [user, user_2, user_3]} do
    {id2, un2} = {user_2.user_id, user_2.username}
    assert {:ok, u} = User.follow(user, id2, un2)
    assert :error = User.follow(u, id2, un2)
    assert {:ok, _u} = User.follow(u, user_3.user_id, user_3.username)
  end

  @tag :load_users
  test "user can't follow self", %{:users => [user, _user_2, _user_3]} do
    assert :error = User.follow(user, user.user_id, user.username)
  end

  @tag :load_users
  test "won't follow non-existing user", %{:users => [user, _user_2, _user_3]} do
    User.follow(user, "id4", "idon'texist")
    :timer.sleep(100)
    assert Mnesia.dirty_match_object({@follows_tab, user.user_id, "id4", "idon'texist"}) == []
  end

  @tag :load_users
  test "blocks user", %{:users => [user, user_2, _user_3]} do
    id1 = user.user_id
    {id2, un2} = {user_2.user_id, user_2.username}
    assert user.blocks == Map.new()
    assert [] == Mnesia.dirty_all_keys(@follows_tab)
    assert {:ok, u} = User.block(user, id2, un2)
    :timer.sleep(100)
    assert Enum.count(u.blocks) == 1
    assert Map.has_key?(u.blocks, id2)
    {:ok, user} = User.init_user(id1)
    assert Enum.count(user.blocks) == 1
    assert Map.has_key?(user.blocks, id2)
    assert [{@blocks_tab, ^id1, ^id2, ^un2}] = Mnesia.dirty_read(@blocks_tab, id1)
  end

  @tag :load_users
  test "unblocks user", %{:users => [user, user_2, _user_3]} do
    id1 = user.user_id
    {id2, un2} = {user_2.user_id, user_2.username}
    {:ok, user} = User.block(user, id2, un2)

    :timer.sleep(100)

    assert [{@blocks_tab, ^id1, ^id2, ^un2}] = Mnesia.dirty_read(@blocks_tab, id1)
    assert {:ok, user} = User.unblock(user, id2, un2)
    :timer.sleep(100)
    assert Enum.count(user.blocks) == 0
    assert [] == Mnesia.dirty_read(@blocks_tab, id1)
    {:ok, user} = User.init_user(id1)
    assert Enum.count(user.blocks) == 0
  end

  @tag :load_users
  test "doesn't block duplicate user", %{:users => [user, user_2, user_3]} do
    {id2, un2} = {user_2.user_id, user_2.username}
    assert {:ok, user} = User.block(user, id2, un2)
    :timer.sleep(100)
    assert :error = User.block(user, id2, un2)
    assert {:ok, _user} = User.block(user, user_3.user_id, user_3.username)
  end

  @tag :load_users
  test "user can't block self", %{:users => [user, _user_2, _user_3]} do
    assert :error = User.block(user, user.user_id, user.username)
  end

  @tag :load_users
  test "doesn't block non-existant users", %{:users => [user, _user_2, _user_3]} do
    User.block(user, "id4", "some_dude")

    :timer.sleep(100)

    assert Mnesia.dirty_match_object({@blocks_tab, user.user_id, "id4", "some_dude"}) == []
  end

  @tag :load_users
  test "deletes user and associations to and from", %{:users => [user, user_2, user_3]} do
    {id1, un1} = {user.user_id, user.username}
    {id2, un2} = {user_2.user_id, user_2.username}
    {id3, un3} = {user_3.user_id, user_3.username}

    {:ok, user} = user |> User.follow(id2, un2) # should be deleted
    {:ok, _user} = user |> User.block(id3, un3) # should be deleted
    {:ok, user_2} = user_2 |> User.follow(id1, un1) # should be deleted
    {:ok, _user_2} = user_2 |> User.block(id3, un3) # should survive!!!
    {:ok, user_3} = user_3 |> User.follow(id1, un1) # should be deleted
    {:ok, _user_3} = user_3 |> User.follow(id2, un2) # should survive!!!

    :timer.sleep(100)

    [^id1, ^id2, ^id3] = Mnesia.dirty_all_keys(@user_tab)
    [{@follows_tab, ^id1, ^id2, ^un2}] = Mnesia.dirty_read(@follows_tab, id1)
    [{@blocks_tab, ^id1, ^id3, ^un3}] = Mnesia.dirty_read(@blocks_tab, id1)
    [{@follows_tab, ^id2, ^id1, ^un1}] = Mnesia.dirty_read(@follows_tab, id2)
    [{@blocks_tab, ^id2, ^id3, ^un3}] = Mnesia.dirty_read(@blocks_tab, id2)
    [{@follows_tab, ^id3, ^id1, ^un1},
      {@follows_tab, ^id3, ^id2, ^un2}] = Mnesia.dirty_read(@follows_tab, id3)

    assert :ok = User.delete(id1)
    :timer.sleep(100)
    assert [^id2, ^id3] = Mnesia.dirty_all_keys(@user_tab)
    assert [] = Mnesia.dirty_read(@follows_tab, id1)
    assert [] = Mnesia.dirty_read(@blocks_tab, id1)
    assert [{@blocks_tab, ^id2, ^id3, ^un3}] = Mnesia.dirty_read(@blocks_tab, id2)
    assert [{@follows_tab, ^id3, ^id2, ^un2}] = Mnesia.dirty_read(@follows_tab, id3)
  end

  @tag :load_users
  test "can't follow or block deleted user", %{:users => [user, user_2, _user_3]} do
    :ok = User.delete(user.user_id)
    :timer.sleep(100)
    user_2 |> User.follow(user.user_id, user.username)
    user_2 |> User.block(user.user_id, user.username)
    # make sure writes didn't go through
    :timer.sleep(100)
    f = {@follows_tab, user_2.user_id, user.user_id, user.username}
    assert Mnesia.dirty_match_object(f) == []
    b = {@blocks_tab, user_2.user_id, user.user_id, user.username}
    assert Mnesia.dirty_match_object(b) == []
  end

  test "returns user on authentication" do
    # TODO: test for successful auth
  end

  test "rejects bad authentication" do
    # TODO: test for both username and password fails
  end

  # TODO: validate password strength? valid email, valid username, valid secret
end
