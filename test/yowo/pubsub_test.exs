defmodule Yowo.PubSubTest do
  use ExUnit.Case, async: true
  alias Yowo.PubSub.Server, as: PubSub

  # NOTE: running this test w/ mix test --no-start is probably wise,
  # otherwise the supervisor will revise the PubSub server

  setup do
    opts = [:duplicate_bag, :named_table, :public, read_concurrency: true, write_concurrency: true]
    # remember: pool_size == 1
    topic_subs =
      0
      |> PubSub.get_topic_table_name()
      |> Yowo.Testing.clear_ets_table()
      |> :ets.new(opts)
    sub_topics =
      0
      |> PubSub.get_sub_table_name()
      |> Yowo.Testing.clear_ets_table()
      |> :ets.new(opts)
    name = PubSub.get_pubsub_server_name(0)
    pubsub =
      case PubSub.start_link(name, sub_topics) do
        {:ok, pid} -> pid
        {:error, {:already_started, pid}} -> pid
      end
    {:ok, %{:pubsub => pubsub, :sub_topics => sub_topics, :topic_subs => topic_subs, :server_name => name}}
  end

  test "subscribes sub and topic in ets", context do
    sub = :fakepid
    topic = "user_id:1"
    {:ok, ^sub} = PubSub.subscribe(topic, sub)
    assert [{^topic, ^sub}] = :ets.lookup(context.topic_subs, topic)
    assert [{^sub, ^topic}] = :ets.lookup(context.sub_topics, sub)
  end

  test "broadcasts msg to subs with topic" do
    f = fn (parent, name)->
        spawn fn ->
          receive do
            msg -> send parent, {name, msg}
          end
        end
      end

    sub_1 = f.(self, :sub_1)
    sub_2 = f.(self, :sub_2)
    sub_3 = f.(self, :sub_3)

    {:ok, ^sub_1} = PubSub.subscribe("topic_1", sub_1)
    {:ok, ^sub_2} = PubSub.subscribe("topic_2", sub_2)
    {:ok, ^sub_3} = PubSub.subscribe("topic_1", sub_3)

    msg = "This is a test."
    PubSub.broadcast("topic_1", msg)
    assert_receive {:sub_1, ^msg}
    assert_receive {:sub_3, ^msg}
    refute_receive {:sub_2, ^msg}
  end

  test "unsubscribes sub from topic and topic from sub in ets", context do
    PubSub.subscribe("topic_1", :sub_1)
    PubSub.subscribe("topic_2", :sub_1)
    PubSub.subscribe("topic_1", :sub_2)

    assert [{"topic_1", :sub_1},{"topic_1", :sub_2}] = :ets.lookup(context.topic_subs, "topic_1")
    assert [{:sub_1, "topic_1"}, {:sub_1, "topic_2"}] = :ets.lookup(context.sub_topics, :sub_1)

    assert :ok = PubSub.unsubscribe("topic_1", :sub_1)
    assert [{"topic_1", :sub_2}] = :ets.lookup(context.topic_subs, "topic_1")
    assert [{:sub_1, "topic_2"}] = :ets.lookup(context.sub_topics, :sub_1)
    assert [{:sub_2, "topic_1"}] = :ets.lookup(context.sub_topics, :sub_2)
  end

  test "removes subscribers on sub crash", context do
    # just making a blocking sub so we can crash it
    sub = spawn fn ->
      receive do
        _msg -> :ok
      end
    end
    PubSub.subscribe("topic", sub)
    assert [{"topic", ^sub}] = :ets.lookup(context.topic_subs, "topic")
    assert [{^sub, "topic"}] = :ets.lookup(context.sub_topics, sub)
    # stop sub, wait until it is actually down
    ref = Process.monitor(sub)
    Process.exit(sub, :test_exit)
    assert_receive {:DOWN, ^ref, :process, ^sub, :test_exit}
    # sync call to make sure :DOWN msg was processed before the ets check
    {:ok, _s} = PubSub.subscribe("bogus", :bogus)
    assert [] = :ets.lookup(context.topic_subs, "topic")
    assert [] = :ets.lookup(context.sub_topics, sub)
  end

  @tag :crash_test
  test "remonitors subscribers after pubsub crash", context do
    sub = spawn fn ->
      receive do
        _msg -> :ok
      end
    end
    PubSub.subscribe("topic", sub)
    # unlink the pubsub server from test process, or it will crash the test!
    # other test processes may still be linked and get the exit?
    # is this why app shuts down? too many exists in timeline for this server?
    Process.unlink(context.pubsub)
    PubSub.stop(context.pubsub)
    assert [{"topic", ^sub}] = :ets.lookup(context.topic_subs, "topic")
    # NOTE: may be already started again by sup if you run w/out no-start
    PubSub.start_link(context.server_name, context.sub_topics)
    ref = Process.monitor(sub)
    Process.exit(sub, :test_exit)
    assert_receive {:DOWN, ^ref, :process, ^sub, :test_exit}
    {:ok, _s} = PubSub.subscribe("bogus", :bogus)
    assert [] = :ets.lookup(context.topic_subs, "topic")
    assert [] = :ets.lookup(context.sub_topics, sub)
  end
end
