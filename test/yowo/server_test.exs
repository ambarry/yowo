defmodule Yowo.ServerTest do
	use ExUnit.Case, async: true

	@port 5000

	defmodule MockSocketController do
		@behaviour Yowo.Server.SocketController

		def start_socket_loop(_server, socket) do
			pid = spawn fn -> recv_socket(socket) end
			{:ok, pid}
		end

		defp recv_socket(_socket) do
			receive do 
				# get tcp message 
				# block on some
				# make sure the other acceptor gets hit, returns right away
				{:tcp, _s, msg} -> 
					case :erlang.binary_to_term(msg) do
						{:block, pid} ->	
							:timer.sleep(5_000)
							send pid, "I'm finally here!"
						{:reply, pid} -> send pid, "I'm here!"
						{:really_block, pid} ->
							:timer.sleep(10_000)
							send pid, "I made it too!"
					end
				_ -> :ok
			end
		end
	end


	setup do
		{:ok, server} = 
			Yowo.Server.Supervisor.start_link(:server_sup_test, @port, 3, MockSocketController, :mocked)
		{:ok, %{:server => server}}
	end

	test "receives connections on multiple acceptors", %{:server => _server} do
		opts = [:binary, packet: 4, active: :once]
		host = 'localhost'
		pid = self()
		{:ok, really_block_client} = :gen_tcp.connect(host, @port, opts)
		:gen_tcp.send(really_block_client, {:really_block, pid} |> :erlang.term_to_binary())
		
		{:ok, block_client} = :gen_tcp.connect(host, @port, opts)
		:gen_tcp.send(block_client, {:block, pid} |> :erlang.term_to_binary())
		
		{:ok, reply_client} = :gen_tcp.connect(host, @port, opts)
		:gen_tcp.send(reply_client, {:reply, pid} |> :erlang.term_to_binary())

		assert_receive("I'm here!")
		assert_receive("I'm finally here!", 5_500)
		refute_receive("I made it too!")
		assert_receive("I made it too!", 10_500)
	end
end



