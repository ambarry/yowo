defmodule Yowo.HapTest do
  use ExUnit.Case, async: false
  alias Yowo.Hap
  alias Yowo.Hap.Database, as: HapDb
  alias :mnesia, as: Mnesia

  @opts Application.get_env(:yowo, :hap)
  @hap_tab @opts[:hap_table]
  @topic_tab @opts[:topic_table]


  setup_all do
    # start hap server in case tests were run with --no-start
    Mnesia.start()
    :ok
  end

  setup do
    # kill tables
    tables = HapDb.tables()
    tables |> Mnesia.wait_for_tables(10_000)
    tables
    |> Enum.each(fn tab -> Mnesia.delete_table(tab) end)

    # re-create tables
    HapDb.init_schema()

    # make sure serer is up, in case a test killed it
    Hap.get_server_name(0) |> Hap.start_link()
    :ok
  end

  test "puts haps" do
    assert [] = Mnesia.dirty_all_keys(@hap_tab)
    assert [] = Mnesia.dirty_all_keys(@topic_tab)
    topics = [
      {10.0, 15.0},
      {12.5, 17.0},
      {89.0, 50.5}
    ]
    expires = Yowo.DateTime.now() + 10_000
    hap = %Hap{
      :hap_id => "SOME_GUID",
      :date_expires => expires,
      :title => "Test Title",
      :message => "This is a test message!!!"}
    id = hap.hap_id
    assert :ok = Hap.put_hap(hap, topics)

    :timer.sleep(100) #TODO: reading too quickly!!!

    assert [id] == Mnesia.dirty_all_keys(@hap_tab)
    assert MapSet.new(topics) == Mnesia.dirty_all_keys(@topic_tab) |> MapSet.new()
    assert [{@hap_tab, ^id, ^hap}] = Mnesia.dirty_read(@hap_tab, id)
    topics
    |> Enum.each(fn t ->
      assert [{@topic_tab, ^t, ^id, ^expires}] = Mnesia.dirty_read(@topic_tab, t)
    end)
  end

  test "gets active haps in topics" do
    act = Yowo.DateTime.now() + 200_000
    inact = Yowo.DateTime.now() - 2_000

    hap = %Hap{:date_expires => act, :title => "A",
      :message => "a_msg", :hap_id => UUID.uuid4()}
    hap_2 = %Hap{:date_expires => act, :title => "B",
      :message => "b_msg", :hap_id => UUID.uuid4()}
    hap_3 = %Hap{:date_expires => inact, :title => "C",
      :message => "c_msg", :hap_id => UUID.uuid4()}
    hap_4 = %Hap{:date_expires => act, :title => "D",
      :message => "d_msg", :hap_id => UUID.uuid4()}
    hap_5 = %Hap{:date_expires => act, :title => "E",
      :message => "e_msg", :hap_id => UUID.uuid4()}
    hap_6 = %Hap{:date_expires => inact, :title => "F",
      :message => "f_msg", :hap_id => UUID.uuid4()}

    [hap, hap_2, hap_3]
    |> Enum.each(fn h -> Hap.put_hap(h, ["user_id_5"]) end)

    [hap_4, hap_5, hap_6]
    |> Enum.each(fn h -> Hap.put_hap(h, [{80.0, 90.0}]) end)

    :timer.sleep(100)

    haps =
      ["user_id_5", {80.0, 90.0}]
      |> Hap.get_active_haps()
    assert is_list(haps)
    assert haps |> Enum.count() == 4
    titles =
      haps
      |> Enum.map(fn h -> h.title end)
      |> MapSet.new()
    assert titles == MapSet.new(["A", "B", "D", "E"])
  end

  test "returns unique haps" do
    id = UUID.uuid4()
    hap = %Hap{:date_expires => Yowo.DateTime.now() + 200_000, :title => "A",
      :message => "a_msg", :hap_id => id}
    topics = [{80.0, 81.8}, {90.2, 90.3}, "some_user_id"]
    

    hap |> Hap.put_hap(topics)

    :timer.sleep(100) #TODO: reading too quickly!!!

    topics
    |> Enum.each(fn t -> assert [{_tab, ^t, ^id, _exp}] = Mnesia.dirty_read(@topic_tab, t) end)
    haps = topics |> Hap.get_active_haps()
    assert haps |> Enum.count() == 1
    assert [^hap] = haps
  end

  test "sets id and expiry on init new hap" do
    m = %{:duration => 100, :title => "Some Title"}
    assert {:ok, hap} = Hap.init_new_hap(m)
    assert hap.hap_id != nil
    now = Yowo.DateTime.now()
    assert hap.date_post > (now - 10)
    assert hap.date_post <= now
    assert hap.date_expires == (hap.date_post + 100)
    assert hap.title == "Some Title"
  end

  test "validates hap on init" do
    # TODO:
  end

  test "generates topics based on lat and lng" do
    # TODO:
    # TODO: maybe change title since user_id is involved?
  end

  # TODO: round location test, etc. (THIS WILL BE USED ELSEWHERE!)
end
