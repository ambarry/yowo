defmodule Yowo.CommandTest do
  use ExUnit.Case, async: true
  import Yowo.{Command, Testing}
  alias Yowo.Packet

  test "valid command strings are matched with correct atoms" do
    "REGISTER"
    |> parse_command
    |> assert_equal({:register, :open})

    "AUTHENTICATE"
    |> parse_command
    |> assert_equal({:authenticate, :open})

    "ARRIVE"
    |> parse_command
    |> assert_equal({:arrive, :auth})

    "FOLLOW"
    |> parse_command
    |> assert_equal({:follow, :auth})

    "INIT_USER"
    |> parse_command
    |> assert_equal({:init_user, :auth})

    "UNFOLLOW"
    |> parse_command
    |> assert_equal({:unfollow, :auth})

    "SAVE_LOCATION"
    |> parse_command
    |> assert_equal({:save_location, :auth})

    "REMOVE_LOCATION"
    |> parse_command
    |> assert_equal({:remove_location, :auth})

    "PUBLISH_HAP"
    |> parse_command
    |> assert_equal({:publish_hap, :auth})

    "LIKE"
    |> parse_command
    |> assert_equal({:like, :auth})

    "FLAG"
    |> parse_command
    |> assert_equal({:flag, :auth})

    "BLOCK_USER"
    |> parse_command
    |> assert_equal({:block_user, :auth})
  end

  test "unknown commands are matched with :unknown_command atom" do
    "BLARGH"
    |> parse_command
    |> assert_equal({:unknown_command, :auth})
  end

  test "nil command returns :unknown_command" do
    nil
    |> parse_command
    |> assert_equal({:unknown_command, :auth})
  end

  test "empty string commands return :unknown_command" do
    ""
    |> parse_command
    |> assert_equal({:unknown_command, :auth})
  end

  test "valid packet returns {:ok, command, data}" do
    data = %Yowo.Hap{}
    cmd = "PUBLISH_HAP"
    packet = %Packet{data: data, command: cmd}
    assert {:ok, {:publish_hap,:auth}, ^data} = decode_command({:ok, packet})
  end

  test "empty message returns error" do
    msg = nil
    assert {:error, :bad_msg} = decode_command(msg)
  end

  test "bad message returns error" do
    msg = %{something: "yes", something_else: "nope"}
    assert {:error, :bad_msg} = decode_command(msg)
  end
end
