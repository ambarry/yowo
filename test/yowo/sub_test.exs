defmodule Yowo.SubTest do
  use ExUnit.Case, async: false
  import Yowo.Testing
  alias Yowo.{Hap, Packet, Sub, User}
  alias Yowo.Hap.Database, as: HapDb
  alias Yowo.User.Database, as: UserDb
  alias Yowo.TotalRecall, as: Recall
  alias :mnesia, as: Mnesia
  alias :sys, as: Sys

  @nodes Application.get_env(:yowo, :nodes)
  @user_opts Application.get_env(:yowo, :user)
  @user_tab @user_opts[:user_table]
  @auth_tab @user_opts[:auth_table]
  @hap_opts Application.get_env(:yowo, :hap)
  @hap_tab @hap_opts[:hap_table]
  @topic_tab @hap_opts[:topic_table]
  @pubsub Application.get_env(:yowo, :pubsub)[:mod]


  defp assert_err(cmd) do
    assert_receive %{:error => ^cmd}
  end

  defp assert_ok(cmd) do
    assert_receive %{:ok => ^cmd}
  end

  setup_all do
    # start mnesia, clear and init tables once
    init_mnesia()
    # start mock pubsub on each node
    # should probably move to each test, in case it gets crashed
    pid = self()
    @nodes 
    |> Enum.each(fn n -> :rpc.call(n, @pubsub, :start_link, [:init, pid]) end)
    #{:ok, _pubsub} = @pubsub.start_link(:init, self())
    # try to start User GenServer in case test is run with --no-start
    # otherwise, already_started, no worries
    User.get_server_name(0) |> User.start_link()
    Hap.get_server_name(0) |> Hap.start_link()
    :ok
  end

  setup context do
    # create a new user per test
    x = :rand.uniform(1000)
    y = :rand.uniform(1000)
    name = "c0c0_#{x}_#{y}"
    {:ok, user} = init_test_user(name)
    
    mock_sock = self()
    {:ok, sub} = Sub.start_link(mock_sock)
    @pubsub.start_link(:add_test, {self(), sub})

    # tear down data on exit
    # TODO: really isolate tests, go async, embrace it, make them independent
    on_exit fn ->
      fn -> Mnesia.dirty_delete(@user_tab, user.user_id) end |> Recall.sync_act()
      fn -> Mnesia.dirty_delete(@auth_tab, name) end |> Recall.sync_act()
      if context[:topic], do: context[:topic] |> clean_haps_for_topic()
      if context[:insert_fol], do: fn -> Mnesia.dirty_delete(@user_tab, context[:insert_fol]) end |> Recall.act()
    end

    if context[:pre_authenticate] do
      packet = %Packet{:command => "AUTHENTICATE",
        :data => %{:username => user.username, :secret => "verySecret5"}}
      send sub, {:tcp, self(), packet}
    end

    if context[:insert_fol] do
      fol_id = context[:insert_fol]
      fn -> Mnesia.write({@user_tab, fol_id, %User{:user_id => fol_id}}) end |> Recall.sync_act()
    end

    if context[:topic] do
      top = context[:topic]
      top |> clean_haps_for_topic()
      {:ok, haps} = top |> setup_haps_for_topic()
      {:ok, %{:sub => sub, :user => user, :haps => haps}}
    else
      {:ok, %{:sub => sub, :user => user}}
    end
  end

  defp setup_haps_for_topic(topic) do
    future = Yowo.DateTime.now() + 20_000
    h = %Hap{:hap_id => "a", :date_expires => future}
    h2 = %Hap{:hap_id => "b", :date_expires => future}
    h3 = %Hap{:hap_id => "c", :date_expires => future}
    haps = [h, h2, h3]
    haps
    |> Enum.each(fn hap ->
      Mnesia.dirty_write({@hap_tab, hap.hap_id, hap})
      Mnesia.dirty_write({@topic_tab, topic, hap.hap_id, future})
    end)
    {:ok, haps}
  end

  defp clean_haps_for_topic(topic) do
    haps = fn -> Mnesia.dirty_read(@topic_tab, topic) end |> Recall.sync_act()
    haps
    |> Enum.each(fn {_tab, _top, id, _exp} -> 
      fn -> Mnesia.dirty_delete(@hap_tab, id) end |> Recall.sync_act()
    end)
    fn -> Mnesia.dirty_delete(@topic_tab, topic) end |> Recall.sync_act()
  end

  defp init_mnesia() do
    :ok = Mnesia.start()
    UserDb.init_schema()
    HapDb.init_schema()
    tables = UserDb.tables() ++ HapDb.tables()
    tables
    |> Mnesia.wait_for_tables(10_000)
    tables
    |> Enum.each(fn t -> fn -> Mnesia.clear_table(t) end |> Recall.sync_act() end)
  end

  defp init_test_user(name) do
    secret = "verySecret5"
    email = name <> "@yowo.com"
    case fn -> Mnesia.read(@auth_tab, name) end |> Recall.sync_act() do
      [{@auth_tab, ^name, _email, id, _salt, _salted_hash}] ->
        fn -> Mnesia.delete(@auth_tab, name) end |> Recall.sync_act()
        fn -> Mnesia.delete(@user_tab, id) end |> Recall.sync_act()
      [] -> :ok
    end
    {:ok, user} = User.create(name, email, secret)
    :timer.sleep(100)
    {:ok, user}
  end

  test "registers new user", %{:sub => sub} do
    pid = self()
    data = %{:username => "bingo77", :email => "bingo77@yowo.com", :secret => "password77"}
    packet = %Packet{:command => "REGISTER", :data => data}
    msg = {:tcp, pid, packet}
    send sub, msg
    assert %{
      :socket => ^pid,
      :user => user,
      :is_authenticated => true,
      :current_location => nil} = Sys.get_state(sub)
    assert user.username == "bingo77"
    assert user.email == "bingo77@yowo.com"
    assert_receive %Yowo.StageResult{
      :stage => :AUTH_COMPLETE, 
      :data => %{:user_id => _, :username => "bingo77"}
    }
  end

  test "returns error on bad registration", %{:sub => sub} do
    pid = self()
    data = %{:username => "bingo77", :email => "b", :secret => "s"}
    packet = %Packet{:command => "REGISTER", :data => data}
    msg = {:tcp, pid, packet}
    send sub, msg
    assert %{
      :socket => ^pid,
      :user => nil,
      :is_authenticated => false,
      :current_location => nil} = Sys.get_state(sub)
    assert_err(:register)
  end

  test "authenticates user and gets initial state", %{:sub => sub, :user => user} do
    packet = %Packet{
      :command => "AUTHENTICATE",
      :data => %{:username => user.username, :secret => "verySecret5"}}
    pid = self()
    send sub, {:tcp, pid, packet}
    assert %{
      :socket => ^pid,
      :user => u,
      :is_authenticated => true,
      :current_location => nil} = Sys.get_state(sub)
    assert u.user_id == user.user_id
  end

  test "doesn't authenticate bad user", %{:sub => sub, :user => user} do
    packet = %Packet{
      :command => "AUTHENTICATE",
      :data => %{:username => user.username, :secret => "NOPE"}}
    pid = self()
    send sub, {:tcp, pid, packet}
    assert %{
      :socket => ^pid,
      :user => nil,
      :is_authenticated => false,
      :current_location => nil} = Sys.get_state(sub)
  end

  test "ignores unauthenticated commands", %{:sub => sub, :user => _user} do
    packet = %Packet{
      :command => "ARRIVE",
      :data => %{:location => {80.0, 80.0}}}
    pid = self()
    send sub, {:tcp, pid, packet}
    assert %{
      :socket => ^pid,
      :user => nil,
      :is_authenticated => false,
      :current_location => nil} = Sys.get_state(sub)
  end

  @tag :pre_authenticate
  test "ignores bad packet", %{:sub => sub, :user => user} do
    id = user.user_id
    pid = self()
    packet = %{:weird => "MESSAGE"}
    send sub, {:tcp, self(), packet}
    assert %{
      :socket => ^pid,
      :user => u,
      :is_authenticated => true,
      :current_location => nil} = Sys.get_state(sub)
    assert u.user_id == id
  end

  @tag :pre_authenticate
  test "ignores bad messages", %{:sub => sub, :user => user} do
    pid = self()
    send sub, :weird_message
    assert %{
      :socket => ^pid,
      :user => u,
      :is_authenticated => true,
      :current_location => nil} = Sys.get_state(sub)
    assert u.user_id == user.user_id
  end

  @tag :pre_authenticate
  test "broadcasts new haps to connection", %{:sub => sub, :user => _user} do
    
    msg = "Hello this is a message."
    hap = %Hap{:message => msg, :hap_id => :a, 
    :date_expires => Yowo.DateTime.now + 10_000}
    send sub, {:broadcast, hap}
    assert_receive ^hap

    send sub, {:broadcast, hap}
    refute_receive ^hap
  end

  @tag :pre_authenticate
  @tag topic: {80.0, 90.0}
  test "arrives at new location", %{:sub => sub, :topic => loc, :haps => haps} do
    pid = self()
    # send sub msg:
    send sub, {:tcp, pid, %Packet{:command => "ARRIVE", :data => %{:location => loc}}}
    # doesn't unsub from current, as == nil:
    refute_receive({:unsub, nil, ^sub})
    # relay pubsub -> receive that sub told pubsub to subscribe to location:
    assert_receive({:sub, ^loc, ^sub})
    # make sure sub has updated loc:
    assert %{:user => u, :current_location => ^loc} = Sys.get_state(sub)
    # make sure sub pulled new haps:
    assert Enum.count(u.haps) == 3
    haps
    |> Enum.reduce(Map.new(), fn(hap, acc) -> Map.put(acc, hap.hap_id, hap.date_expires) end)
    |> assert_equal(u.haps)
    # relay connection -> make sure sub sends new haps over connection!
    [h, h2, h3] = haps
    assert_receive(^h)
    assert_receive(^h2)
    assert_receive(^h3)
  end

  @tag :pre_authenticate
  @tag topic: {81.0, 91.0}
  test "ignores arriving at same location", %{:sub => sub, :topic => loc, :haps => haps} do
    pid = self()
    packet = %Packet{:command => "ARRIVE", :data => %{:location => loc}}
    send sub, {:tcp, pid, packet}
    refute_receive({:unsub, nil, ^sub})
    assert_receive({:sub, ^loc, ^sub})
    assert %{:user => u, :current_location => ^loc} = Sys.get_state(sub)
    assert Enum.count(u.haps) == 3
    # relay connection -> make sure sub sends new haps over connection!
    [h, h2, h3] = haps
    assert_receive(^h)
    assert_receive(^h2)
    assert_receive(^h3)
    # do it again and make sure nothing happens!
    send sub, {:tcp, pid, packet}
    refute_receive({:unsub, ^loc, ^sub})
    refute_receive({:sub, ^loc, ^sub})
    assert %{:user => u, :current_location => ^loc} = Sys.get_state(sub)
    assert Enum.count(u.haps) == 3
    refute_receive(^h)
    refute_receive(^h2)
    refute_receive(^h3)
  end

  @tag :pre_authenticate
  @tag topic: {23.0, 93.0}
  test "unsubscribe from previous location if not saved",%{:sub => sub, :topic => loc} do
    pid = self()
    packet = %Packet{:command => "ARRIVE", :data => %{:location => loc}}
    send sub, {:tcp, pid, packet}
    refute_receive({:unsub, nil, ^sub})
    assert_receive({:sub, ^loc, ^sub})
    assert %{:current_location => ^loc} = Sys.get_state(sub)
    loc2 = {10.0, 12.0}
    packet = %Packet{:command => "ARRIVE", :data => %{:location => loc2}}
    send sub, {:tcp, pid, packet}
    assert_receive({:unsub, ^loc, ^sub})
    assert_receive({:sub, ^loc2, ^sub})
    assert %{:current_location => ^loc2} = Sys.get_state(sub)
  end

  @tag :pre_authenticate
  @tag topic: {11.0, 11.0}
  test "saves location and gets new haps", %{:sub => sub, :topic => loc, :haps => haps} do
      pid = self()
      packet = %Packet{:command => "SAVE_LOCATION", :data => %{location: loc}}
      send sub, {:tcp, pid, packet}
      assert_receive({:sub, ^loc, ^sub})
      refute_receive({:unsub, _l, _s})
      assert %{:user => u} = Sys.get_state(sub)
      assert Enum.count(u.haps) == 3
      [h1, h2, h3] = haps
      assert_receive(^h1)
      assert_receive(^h2)
      assert_receive(^h3)
  end


  test "doesn't save duplicate locations" do
    # TODO: doesn't save duplicate locations
  end

  @tag :pre_authenticate
  @tag topic: {15.0, 15.5}
  test "doesn't subscribe when saving current location", %{:sub => sub, :topic => loc, :haps => haps} do
    pid = self()
    packet = %Packet{:command => "ARRIVE", :data => %{:location => loc}}
    send sub, {:tcp, pid, packet}

    assert_receive({:sub, ^loc, ^sub})
    assert %{:user => u, :current_location => ^loc} = Sys.get_state(sub)
    assert Enum.count(u.haps) == 3
    [h1, h2, h3] = haps
    assert_receive(^h1)
    assert_receive(^h2)
    assert_receive(^h3)

    packet = %Packet{:command => "SAVE_LOCATION", :data => %{location: loc}}
    send sub, {:tcp, pid, packet}
    refute_receive({:sub, ^loc, ^sub})
    refute_receive(^h1)
    refute_receive(^h2)
    refute_receive(^h3)
    assert %{:user => u, :current_location => ^loc} = Sys.get_state(sub)
    assert Enum.count(u.haps) == 3
  end

  @tag :pre_authenticate
  @tag topic: {82.0, 92.0}
  test "doesn't subscribe when arriving at saved location", %{:sub => sub, :topic => loc, :haps => haps} do
      pid = self()
      packet = %Packet{:command => "SAVE_LOCATION", :data => %{location: loc}}
      send sub, {:tcp, pid, packet}

      assert_receive({:sub, ^loc, ^sub})
      assert %{:user => u, :current_location => nil} = Sys.get_state(sub)
      assert Enum.count(u.haps) == 3
      [h1, h2, h3] = haps
      assert_receive(^h1)
      assert_receive(^h2)
      assert_receive(^h3)

      packet = %Packet{:command => "ARRIVE", :data => %{:location => loc}}
      send sub, {:tcp, pid, packet}
      refute_receive({:sub, ^loc, ^sub})
      refute_receive(^h1)
      refute_receive(^h2)
      refute_receive(^h3)
      assert %{:user => u, :current_location => ^loc} = Sys.get_state(sub)
      assert Enum.count(u.haps) == 3
  end

  @tag :pre_authenticate
  test "keeps subscription to old location if saved", %{:sub => sub} do
    pid = self()
    # save the location
    loc = {25.0, 90.8}
    packet = %Packet{:command => "SAVE_LOCATION", :data => %{:location => loc}}
    send sub, {:tcp, pid, packet}
    assert_receive({:sub, ^loc, ^sub})
    assert %{:user => u, :current_location => nil} = Sys.get_state(sub)
    assert Enum.count(u.locations) == 1

    # arrive so it becomes current location too
    packet = %Packet{:command => "ARRIVE", :data => %{:location => loc}}
    send sub, {:tcp, pid, packet}
    refute_receive({:sub, ^loc, ^sub})
    assert %{:user => u, :current_location => ^loc} = Sys.get_state(sub)
    assert Enum.count(u.locations) == 1

    # depart from current
    loc2 = {30.0, 30.0}
    packet = %Packet{:command => "ARRIVE", :data => %{:location => loc2}}
    send sub, {:tcp, pid, packet}
    assert_receive({:sub, ^loc2, ^sub})
    # make sure you didn't unsubscribe!!!
    refute_receive({:unsub, ^loc, ^sub})
    assert %{:user => u, :current_location => ^loc2} = Sys.get_state(sub)
    assert Enum.count(u.locations) == 1
  end

  test "stops sub on tcp close", %{:sub => sub} do
    ref = Process.monitor(sub)
    send sub, {:tcp_closed, self()}
    assert_receive({:DOWN, ^ref, :process, ^sub, :normal})
    assert !Process.alive?(sub)
  end

  test "stops on tcp error", %{:sub => sub} do
    ref = Process.monitor(sub)
    send sub, {:tcp_error, self(), :whaaaaat}
    assert_receive({:DOWN, ^ref, :process, ^sub, :normal})
    assert !Process.alive?(sub)
  end

  @tag :pre_authenticate
  test "removes location", %{:sub => sub} do
    pid = self()
    loc = {25.0, 90.8}
    # save a location
    packet = %Packet{:command => "SAVE_LOCATION", :data => %{:location => loc}}
    send sub, {:tcp, pid, packet}
    assert_receive({:sub, ^loc, ^sub})
    assert %{:user => u} = Sys.get_state(sub)
    assert Enum.count(u.locations) == 1
    # remove it and make sure it's gone!
    packet = %Packet{:command => "REMOVE_LOCATION", :data => %{:location => loc}}
    send sub, {:tcp, pid, packet}
    assert_receive({:unsub, ^loc, ^sub})
    assert %{:user => u} = Sys.get_state(sub)
    assert Enum.count(u.locations) == 0
  end

  @tag :pre_authenticate
  test "ignores removing bad location", %{:sub => sub} do
    pid = self()
    # save a location
    loc = {90.0, 90.0}
    packet = %Packet{:command => "SAVE_LOCATION", :data => %{:location => loc}}
    send sub, {:tcp, pid, packet}
    assert_receive({:sub, ^loc, ^sub})
    assert %{:user => u} = Sys.get_state(sub)
    assert Enum.count(u.locations) == 1

    # try to remove one that the user doesn't have
    packet = %Packet{:command => "REMOVE_LOCATION", :data => %{:location => {88.5, 88.5}}}
    send sub, {:tcp, pid, packet}
    refute_receive({:unsub, _2nd_loc, ^sub})
    assert %{:user => u} = Sys.get_state(sub)
    assert Enum.count(u.locations) == 1
    assert MapSet.member?(u.locations, loc)
  end

  @tag :pre_authenticate
  test "doesn't unsubscribe from location if current", %{:sub => sub} do
    pid = self()
    # arrive
    loc = {33.9, 47.2}
    a_packet = %Packet{:command => "ARRIVE", :data => %{:location => loc}}
    send sub, {:tcp, pid, a_packet}
    assert_receive({:sub, ^loc, ^sub})
    assert %{:user => u, :current_location => ^loc} = Sys.get_state(sub)
    assert Enum.count(u.locations) == 0
    # save it
    s_packet = %{a_packet | :command => "SAVE_LOCATION"}
    send sub, {:tcp, pid, s_packet}
    refute_receive({:sub, ^loc, ^sub})
    assert %{:user => u, :current_location => ^loc} = Sys.get_state(sub)
    assert Enum.count(u.locations) == 1
    # depart from location
    loc2 = {55.0, 55.0}
    a2_packet = %{a_packet | :data => %{:location => loc2}}
    send sub, {:tcp, pid, a2_packet}
    # but make sure still saved
    refute_receive({:unsub, ^loc, ^sub})
    assert %{:user => u, :current_location => ^loc2} = Sys.get_state(sub)
    assert Enum.count(u.locations) == 1
    assert MapSet.member?(u.locations, loc)
  end

  @tag :pre_authenticate
  @tag topic: "some-user-5867-Guid"
  @tag insert_fol: "some-user-5867-Guid"
  test "follows user", %{:sub => sub, :topic => id, :haps => haps} do
    pid = self()
    name = "bartholomew5"
    pak = %Packet{
      :command => "FOLLOW",
      :data => %{:follow_id => id, :follow_name => name}
    }
    send sub, {:tcp, pid, pak}
    # make sure you subscribe
    assert_receive {:sub, ^id, ^sub}
    # make sure you get the haps!
    [h1, h2, h3] = haps
    assert_receive(^h1)
    assert_receive(^h2)
    assert_receive(^h3)
    assert %{:user => u} = Sys.get_state(sub)
    assert Enum.count(u.haps) == 3
    # make sure you add the user!
    assert Map.has_key?(u.follows, id)
  end

  @tag :pre_authenticate
  @tag insert_fol: "another-user-5867-Guid"
  test "doesn't follow duplicate user", %{:sub => sub, :insert_fol => id} do
    pid = self()
    name = "yupimapersonYYYYup"
    pak = %Packet{
      :command => "FOLLOW",
      :data => %{:follow_id => id, :follow_name => name}
    }
    send sub, {:tcp, pid, pak}
    assert_receive {:sub, ^id, ^sub}
    assert %{:user => u} = Sys.get_state(sub)
    assert Map.has_key?(u.follows, id)

    send sub, {:tcp, pid, pak}
    refute_receive {:sub, ^id, ^sub}
    assert %{:user => u} = Sys.get_state(sub)
    assert Map.has_key?(u.follows, id)
  end

  @tag :pre_authenticate
  @tag insert_fol: "third_Guid-something_YES"
  test "removes followed user", %{:sub => sub, :insert_fol => id} do
    pid = self()
    # follow
    name = "whatisthewhatisTHE"
    pak = %Packet{
      :command => "FOLLOW",
      :data => %{:follow_id => id, :follow_name => name}
    }
    send sub, {:tcp, pid, pak}
    assert_receive {:sub, ^id, ^sub}
    assert_ok :follow
    assert %{:user => u} = Sys.get_state(sub)
    assert Map.has_key?(u.follows, id)
    # unfollow!
    pak = %Packet{
      :command => "UNFOLLOW",
      :data => %{:follow_id => id, :follow_name => name}
    }
    send sub, {:tcp, pid, pak}
    assert_receive {:unsub, ^id, ^sub}
    assert_ok :unfollow
    assert %{:user => u} = Sys.get_state(sub)
    refute Map.has_key?(u.follows, id)
  end

  @tag :pre_authenticate
  @tag insert_fol: "yet-another-something_YES"
  test "ignores removing bad user", %{:sub => sub, :insert_fol => id} do
    # unfollow a rando
    pak = %Packet{
      :command => "UNFOLLOW",
      :data => %{:follow_id => id, :follow_name => "whatwhat"}
    }
    send sub, {:tcp, self(), pak}
    refute_receive {:unsub, ^id, ^sub}
    :unfollow |> assert_err
  end 

  @tag :pre_authenticate
  test "doesn't follow non-existent user", %{:sub => sub} do
    id = "this-guy-has-no-account"
    pak = %Packet{
      :command => "FOLLOW",
      :data => %{:follow_id => id, :follow_name => "NOPE"}
    }
    send sub, {:tcp, self(), pak}
    
    # TODO: once GenServer starts sending back errors...
    #:follow |> assert_err
  end

  @tag :pre_authenticate
  test "publishes hap", %{:sub => sub, :user => user} do
    # cheat a little: need a key for the relay for broadcasts
    # since it is sent through task process, not the test (pid won't locate)
    # may need to pattern match if we add more broadcast tests that hit
    Process.register(self(), :bcast_test)
    @nodes
    |> Enum.each(fn n -> 
      :rpc.call(n, @pubsub, :start_link, [:add_broadcast_test, {:bcast_test, node(), :bcast}]) 
    end)

    title = "Hap Title Yes!"
    message = "Isn't this a sweet hap?!?"
    lo = {10.0, 10.0}

    pak = %Packet{
      :command => "PUBLISH_HAP",
      :data => %{
        :title => title,
        :message => message,
        :location => lo
      }
    }

    send sub, {:tcp, self(), pak}
    %{:user_id => id, :username => name} = user
    {:ok, tops} =
      pak.data
      |> Map.put(:user_id, id)
      |> Hap.generate_topics()


    @nodes # should receive once per node
    |> Enum.each(fn _n -> 
      assert_receive({:broadcast, topics, msg})
      assert MapSet.new(topics) == MapSet.new(tops)
      assert {
        :broadcast,
        %Hap{
          :user_id => ^id,
          :username => ^name,
          :location => ^lo,
          :hap_id => some_guid,
          :date_post => some_date_post,
          :date_expires => some_date_expires,
          :duration => 3600,
          :category => :general
        }
      } = msg
      assert some_guid != nil
      assert some_date_post != nil
      assert some_date_expires == some_date_post + 3600 
    end) 
    
    :publish_hap |> assert_ok
  end

  @tag :pre_authenticate
  test "sends error on bad hap", %{:sub => sub} do
    bad_pak = %Packet{:command => "PUBLISH_HAP", :data => %{:nope => "Nope!"}}
    send sub, {:tcp, self(), bad_pak}
    refute_receive {:broadcast, _t, _msg}
    :publish_hap |> assert_err
  end

  # TODO: handle bad publishes, blocks? likes? (eventually)


end
