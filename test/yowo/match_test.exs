defmodule Yowo.MatchTest do
  use ExUnit.Case, async: true
  alias :yowo_query, as: Query

  setup do
    now = :erlang.universaltime()
    past = (now |> :calendar.datetime_to_gregorian_seconds()) - 20000
    future = (now |> :calendar.datetime_to_gregorian_seconds()) + 20000
    data = %{past: {:sample, 1,  past}, future: {:sample, 2,  future}}

    :ets.new(:users, [:set, :named_table])
    a = {1, "Alfred"}
    b = {2, "Barnes"}
    c = {3, "Crook"}
    :ets.insert(:users, a)
    :ets.insert(:users, b)
    :ets.insert(:users, c)

    {:ok, %{:data => data, :user_set => MapSet.new([a,b,c])}}
  end

  test "matches where date is greater than now", %{:data => data, :user_set => _u} do
    now = :erlang.universaltime() |> :calendar.datetime_to_gregorian_seconds()
    match_head = {:sample, :"$1", :"$2"}
    guard = {:">", :"$2", now}
    result = :"$1"
    assert {:ok, res} = :ets.test_ms(data.future, [{match_head, [guard], [result]}])
    assert res == 2
  end

  test "query successfully fetches records", %{:data => _data, :user_set => users} do
    # NOTE: this will fail -> can't use erlang list comps here...
    res = Query.test_query(:users)
    assert Enum.count(res) == 3
    assert MapSet.new(res) == users
  end

  test "query contains fetches correct records", %{:data => _data, :user_set => users} do
    x = [1,2]
    res = Query.test_contains(:users, x)
    assert Enum.count(res) == 2
    assert MapSet.new(res) == MapSet.delete(users, {3, "Crook"})
  end

  test "join and filter on date", %{:data => data, :user_set => users} do
    now = :erlang.universaltime() |> :calendar.datetime_to_gregorian_seconds()
    :ets.new(:topic_users, [:bag, :named_table])
    {:sample, 1, past} = data.past
    {:sample, 2, future} = data.future
    :ets.insert(:topic_users, {1, past})
    :ets.insert(:topic_users, {2, future})
    :ets.insert(:topic_users, {3, future})
    res = Query.test_join(:users, :topic_users, now)
    assert Enum.count(res) == 2
    assert MapSet.new(res) == MapSet.delete(users, {1, "Alfred"})
  end
end
