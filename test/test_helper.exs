defmodule Yowo.Testing do
  import ExUnit.Assertions

  def assert_equal(a, b) do
    assert(a == b)
  end

  def clear_ets_table(table) do
    try do
      :ets.delete(table)
      table
    rescue
      ArgumentError -> table
    end
  end
end


exclude_distrib = if Node.alive?, do: [], else: [distributed: true]
exclude_crashes = [crash_test: true]
exclude = exclude_distrib ++ exclude_crashes
ExUnit.start(exclude: exclude)
