use Mix.Config

  config :logger, level: :info

  config :yowo, :mnesia_dir, "D:/__DEV/__data/yowo"
  config :yowo, :nodes, [node()]

  config :yowo, :connection, 
  	mod: Yowo.Connection.Socket

  config :yowo, :pubsub, 
  	mod: Yowo.PubSub.Server,
  	pool_size: 3

  config :yowo, :user,
  	user_table: :user,
  	auth_table: :auth,
  	follows_table: :user_follows,
    blocks_table: :user_blocks,
	  pool_size: 3,
    frags: 4

  config :yowo, :hap,
  	hap_table: :haps,
  	topic_table: :topics,
    pool_size: 3,
    frags: 4