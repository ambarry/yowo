use Mix.Config

  config :logger, level: :debug

  config :yowo, :mnesia_dir, "D:/__DEV/__data/yowo"
  #config :yowo, :nodes, [:foo@sager, :bar@sager] #[node()]
  config :yowo, :nodes, [node()]
  config :yowo, :connection, 
  	mod: Yowo.Connection.Mock

  config :yowo, :pubsub, 
  	mod: Yowo.PubSub.Mock,
  	pool_size: 1

  config :yowo, :user,
  	user_table: :user_test,
  	auth_table: :auth_test,
  	follows_table: :user_follows_test,
    blocks_table: :user_blocks_test,
	  pool_size: 1,
    frags: 1

  config :yowo, :hap,
  	hap_table: :haps_test,
  	topic_table: :topics_test,
    pool_size: 1,
    frags: 1