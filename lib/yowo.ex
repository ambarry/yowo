defmodule Yowo do
  use Application
  require Logger

  def start(_type, _args) do
    Logger.info "Starting YOWO!!!"
    
  	case node() do # hack for now
  		:bar@sager -> 4041
  		_ -> 4040
  	end
    |> Yowo.Supervisor.start_link() # todo: grab args from cmd line?
  end
end
