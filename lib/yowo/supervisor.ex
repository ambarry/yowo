defmodule Yowo.Supervisor do
  use Supervisor
  require Logger
  alias :mnesia, as: Mnesia
  alias Yowo.User.Database, as: UserDb
  alias Yowo.Hap.Database, as: HapDb


  @nodes Application.get_env(:yowo, :nodes)
  @mnesia_dir Application.get_env(:yowo, :mnesia_dir)
  @hap_supervisor Yowo.Hap.Supervisor
  @pubsub_supervisor Yowo.PubSub.Supervisor
  @sub_supervisor Yowo.Sub.Supervisor
  @user_supervisor Yowo.User.Supervisor
  @server_supervisor Yowo.Server.Supervisor
  @dispatcher_tasks Yowo.DispatcherTasks
  @num_acceptors 10 # TODO: pull from config
  

  def start_link(port) do
    Logger.info "About to start main supervisor"
    Supervisor.start_link(__MODULE__, {:ok, port})
  end

  def init({:ok, port}) do
    :ok = init_db()
    children = [
      supervisor(Yowo.Hap.Supervisor, [@hap_supervisor], []),
      supervisor(Yowo.User.Supervisor, [@user_supervisor], []),
      supervisor(Yowo.PubSub.Supervisor, [@pubsub_supervisor], []),
      supervisor(Yowo.Sub.Supervisor, [@sub_supervisor], []),
      supervisor(Task.Supervisor, [[name: @dispatcher_tasks]]),
      supervisor(Yowo.Server.Supervisor, 
        [@server_supervisor, port, @num_acceptors, @sub_supervisor, @sub_supervisor], [])
      #worker(Task, [Yowo.Server, :accept, [port, &Yowo.Sub.Supervisor.start_sub/2, @sub_supervisor]])
    ]
    supervise(children, strategy: :one_for_one)
  end

  def init_db do
    # TODO: this maybe should be an optional flag on start?
    #Application.load(:mnesia)
    #Application.put_env(:mnesia, :dir, @mnesia_dir)
    :ok = Mnesia.start()
    Mnesia.create_schema(@nodes)
    UserDb.init_schema()
    HapDb.init_schema()
    UserDb.tables() ++ HapDb.tables() |> Mnesia.wait_for_tables(10_000)
    :ok
  end
end
