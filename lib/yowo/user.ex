defmodule Yowo.User do
	use GenServer
	require Logger
  alias :crypto, as: Crypto
  alias Yowo.{Dispatcher, Hap, Pooler}
  alias Yowo.User.Database, as: UserDb


	@moduledoc """
		Handles user registration, authentication, and 
		maintains relevant app info per user.
		Handles all writes to User db through GenServer (calls for now).
		Reads are publicly accessible.
	"""

	@opts Application.get_env(:yowo, :user)
	@pool_size @opts[:pool_size]


	defstruct [
    user_id: "",
    username: "",
    email: "",
    locations: MapSet.new(), # set of {lat, lng}
    follows: Map.new(), # map of user_id => username
    haps: Map.new(), # map of hap_id => date_expires
    blocks: Map.new() # map of follow_id => follow_name
  ]

  # Set up pooling.
  # NOTE: don't need to worry about table names, only servers,
	# as unlike PubSub, here you're using Mnesia 
	# to handle the table frags (or will be...)


	def get_server_name(index), do: Pooler.get_server_name("Yowo.User.Server", index)
	defp get_server(user_id), do: Pooler.resolve(user_id, @pool_size, &get_server_name/1)


	# NOTE: separate reads from writes! ALWAYS
	# Be sure not to overwrite user w/ db copy when 
	# passed in from sub. The sub version may contain non-db info.
	# Only update what was requested.
	# TODO: consider trying to convert to casts, replying when you can
	# But need a plan to eal with out-of-order messages in client
	# (OR, just cast and assume success :)) -> give up C in CAP
	# But see if performance is actually an issue first

	def start_link(name) do
		Logger.info "starting User Server: #{name}..."
		GenServer.start_link(__MODULE__, :ok, [name: name])
	end

	def init(:ok) do
		{:ok, %{}}
	end

	def lookup(id) do
		UserDb.lookup(id)
	end

	def create(username, email, secret) do
    # TODO: validate username and email? e.g. regex prior to send
    # NOTE: really need to wait on this one for a result. 
    # GenServer is a call
    # Awaiting the task response from whatever node this hits
    cond do
      !valid_username?(username) -> {:error, :bad_username}
      !valid_email?(email) -> {:error, :bad_email}
      !valid_password?(secret) -> {:error, :bad_password}
      true -> 
        user = %Yowo.User{
          :user_id => UUID.uuid4(), 
          :username => username, 
          :email => email
        }
        salt = Crypto.strong_rand_bytes(16)
        salted_hash = salt <> hash(secret)
        res = 
          user.user_id
          |> Dispatcher.async(Yowo.User, :do_create, [user, salt, salted_hash])
          |> Task.await()
        case res do
          :ok -> {:ok, user}
          err -> err
        end    
    end
	end

  defp valid_email?(email) do
    ~r/^[\w.-]{1,50}@[\w.-]{1,50}\.[a-zA-Z]{2,4}$/ |> Regex.match?(email)
  end

  defp valid_username?(username) do
    ~r/^\w{3,15}$/ |> Regex.match?(username)
  end

  defp valid_password?(pword) do
    ~r/^[\w\(\)-.]{5,256}$/ |> Regex.match?(pword)
  end

  def do_create(user, salt, salted_hash) do
    user.user_id
    |> get_server()
    |> GenServer.call({:create, user, salt, salted_hash}) 
  end

	def init_user(id) do
		UserDb.init_user(id)
	end

	def authenticate(username, secret) do
    case UserDb.find_auth(username) do
      {:ok, {id, salt, salted_hash}} ->
        h = hash(secret)
        cond do
         salted_hash == salt <> h -> id |> init_user()
         true -> 
          Logger.debug("failed on hash match")
          {:error, :bad_auth}
        end
      :error -> 
        Logger.debug("failed to find auth by username")
        {:error, :bad_auth}
    end
  end

  def put_hap(user, hap) do
  	cond do
      has_hap?(user, hap.hap_id) -> :error
      true ->
        _task = Dispatcher.async(user.user_id, Yowo.User, :do_put_hap, [user.user_id, hap])
        haps = 
          user.haps
          |> Hap.filter_haps()
          |> Map.put(hap.hap_id, hap.date_expires)
        user = Map.put(user, :haps, haps)
        {:ok, user}
    end
  end

  def do_put_hap(user_id, hap) do
    user_id
    |> get_server()
    |> GenServer.cast({:put_hap, user_id, hap})
  end

  def put_haps(user, haps) when is_list(haps) do
    _task = Dispatcher.async(user.user_id, Yowo.User, :do_put_haps, [user.user_id, haps])
    user_haps = 
      haps
      |> UserDb.consolidate_haps(user.haps)
      |> Hap.filter_haps()
    {:ok, Map.put(user, :haps, user_haps)}
  end

  def has_hap?(user, hap_id) do
    Map.has_key?(user.haps, hap_id)
  end

  def do_put_haps(user_id, haps) do
    user_id
    |> get_server() 
    |> GenServer.cast({:put_haps, user_id, haps})
  end

  def put_location(user, location) do
  	cond do
      has_location?(user, location) -> :error
      true ->
        _task = Dispatcher.async(user.user_id, Yowo.User, :do_put_location, 
          [user.user_id, location])
        locs = MapSet.put(user.locations, location)
        {:ok, Map.put(user, :locations, locs)}
    end
  end

  def do_put_location(user_id, location) do
    user_id
    |> get_server() 
    |> GenServer.cast({:put_location, user_id, location})
  end

  def has_location?(user, location) do
    location in user.locations
  end

  def remove_location(user, location) do
  	cond do
      has_location?(user, location) ->
        # TODO: consider, move to sub? may be too coupled to dispatcher here...
        # or let pubsub dispatch too, and keep sub cleaner?
        # dispatch: 
        _task = Dispatcher.async(user.user_id, Yowo.User, :do_remove_location, 
          [user.user_id, location])
        locs = MapSet.delete(user.locations, location)
        {:ok, Map.put(user, :locations, locs)}
      true -> :error
    end
  end

  def do_remove_location(user_id, location) do
    user_id
    |> get_server() 
    |> GenServer.cast({:remove_location, user_id, location})
  end

  def follow(user, follow_id, follow_name) do
  	cond do
      user.user_id == follow_id -> :error
      follows?(user, follow_id) -> :error
      true ->
        :ok = 
          user.user_id
    			|> get_server()
      		|> GenServer.cast({:follow, user.user_id, follow_id, follow_name})
        fols = Map.put(user.follows, follow_id, follow_name)
        {:ok, Map.put(user, :follows, fols)}
    end
  end

  def unfollow(user, follow_id, follow_name) do
		cond do
      follows?(user, follow_id) ->
        :ok = 
          user.user_id
  				|> get_server()
    			|> GenServer.cast({:unfollow, user.user_id, follow_id, follow_name})
        fols = Map.delete(user.follows, follow_id)
        {:ok, Map.put(user, :follows, fols)}
      true -> :error
    end  	
  end

  def follows?(user, follow_id) do
    Map.has_key?(user.follows, follow_id)
  end

  def block(user, block_id, block_name) do
  	cond do
      user.user_id == block_id -> :error
      blocks?(user, block_id) -> :error
      true ->
        :ok = 
          user.user_id
  				|> get_server()
    			|> GenServer.cast({:block, user.user_id, block_id, block_name})
        blocks = Map.put(user.blocks, block_id, block_name)
        {:ok, Map.put(user, :blocks, blocks)}
    end
  end

  def blocks?(user, block_id) do
    Map.has_key?(user.blocks, block_id)
  end

  def unblock(user, block_id, block_name) do
		cond do
      blocks?(user, block_id) ->
        :ok = 
          user.user_id
  				|> get_server()
  				|> GenServer.cast({:unblock, user.user_id, block_id, block_name})
        blocks = Map.delete(user.blocks, block_id)
        {:ok, Map.put(user, :blocks, blocks)}
      true -> :error
    end
  end

  def delete(user_id) do
  	user_id
  	|> get_server()
  	|> GenServer.cast({:delete, user_id})
  end

	defp hash(secret) do
    Crypto.hash(:sha256, secret)
  end

	def stop(server) do
		GenServer.stop(server, :stop)
	end


	# All writes are handled by GenServer.
	# Ideally we could switch to casts. This way, the end user can still
	# receive messages while a write takes place.
	# This may be tough, since we're expecting the user to be returned in latest state,
	# and this will be thrown off if messages don't come in in order.
	# May try to reconcile later, but this is really overoptimizing.
	# For now, call.
	# Could later try to be optimistic on sub. Then, when reply comes in, could ignore
	# if success (maybe don't even send!)
	# still risks if multi-message..

  # TODO: pattern match on writes, handle error / no_exists

	def handle_call({:create, user, salt, salted_hash}, _from, state) do
    Logger.debug "user GenServer: new user: handling (call) :create"
		{:reply, UserDb.create_user(user, salt, salted_hash), state}
	end

	def handle_cast({:put_hap, user_id, hap}, state) do
    # TODO: handle no exists vs ok
    Logger.debug "user GenServer: user_id #{user_id}: handling (cast) :put_hap"
		UserDb.write_hap(user_id, hap)
    Logger.debug "wrote hap to db, not going to reply..."
		{:noreply, state}
	end

	def handle_cast({:put_haps, user_id, haps}, state) do
    # TODO: handle no exists vs ok
    Logger.debug "user GenServer: user_id #{user_id}: handling (cast) :put_haps"
		UserDb.write_haps(user_id, haps)
    {:noreply, state}
	end

	def handle_cast({:put_location, user_id, location}, state) do
    # TODO: handle no exists vs ok
    Logger.debug "user GenServer: user_id #{user_id}: handling (cast) :put_location"
		UserDb.write_location(user_id, location)
    {:noreply, state}
	end

	def handle_cast({:remove_location, user_id, location}, state) do
    # TODO: handle no exists vs ok?
    Logger.debug "user GenServer: user_id #{user_id}: handling (cast) :remove_location"
		UserDb.delete_location(user_id, location)      
    {:noreply, state}
	end

	def handle_cast({:follow, user_id, follow_id, follow_name}, state) do
    Logger.debug "user GenServer: user_id #{user_id}: handling (cast) :follow"
    case UserDb.write_follow(user_id, follow_id, follow_name) do
      :ok -> {:noreply, state}
      {:error, :no_exists} -> {:noreply, state} # TODO: send back!
    end
	end

	def handle_cast({:unfollow, user_id, follow_id, follow_name}, state) do
    Logger.debug "user GenServer: user_id #{user_id}: handling (cast) :unfollow"
    :ok = UserDb.delete_follow(user_id, follow_id, follow_name)
    {:noreply, state}
	end

	def handle_cast({:block, user_id, block_id, block_name}, state) do
    Logger.debug "user GenServer: user_id #{user_id}: handling (cast) :block"
    case UserDb.write_block(user_id, block_id, block_name) do
      :ok -> {:noreply, state}
      {:error, :no_exists} -> {:noreply, state} # TODO:
    end
	end

	def handle_cast({:unblock, user_id, block_id, block_name}, state) do
    Logger.debug "user GenServer: user_id #{user_id}: handling (cast) :unblock"
    :ok = UserDb.delete_block(user_id, block_id, block_name)
    {:noreply, state}
	end

	def handle_cast({:delete, id}, state) do
    Logger.debug "user GenServer: handling (cast) :delete"
    :ok = UserDb.delete_user(id)
		{:noreply, state}
	end

	def handle_info(_, state) do
    Logger.debug "user GenServer: UNKNOWN message"
		{:noreply, state}
	end
end
