defmodule Yowo.Connection.Socket do
  @behaviour Yowo.Connection
  require Logger

  @moduledoc """
  Module for reading / writing from socket.
  Not really hanlding reading right now, as just passed as Erlang message.
  Implements the Connection behaviour so we can inject.
  """


  @doc """
    Serializes and writes a message
    to the client socket.
  """
  @spec send_contents(:gen_tcp.socket, any) :: :ok | {:error, atom}
  Logger.debug "[socket] sending contents..."
  def send_contents(socket, msg) do
    Logger.debug "[socket] sending contents..."
    # TODO: should we match for error? would need to rescue in serialize...
    {:ok, data} = serialize msg
    case :gen_tcp.send(socket, data) do
      :ok -> Logger.debug "[socket] contents send!!!"
      {:error, reason} -> Logger.debug "[socket] error sending: #{reason}"
    end
  end


  # TODO: list version!



  @doc """
    Actives socket. Really, reactiating in cases
    where we set opts to :once to control msg rates.
  """
  @spec maintain_conn(:gen_tcp.socket) :: :ok | {:error, :inet.posix}
  def maintain_conn(socket) do
    :inet.setopts(socket, [active: :once])
  end

  @doc """
    Serializes data to ETF for socket transmission.
  """
  def serialize(data) do
    {:ok, :erlang.term_to_binary(data)}
  end

  @doc """
    Straight call to Erlang's ETF conversion.
    Throws an exception if contents aren't a valid term.
  """
  def deserialize!(bin) do
    {:ok, :erlang.binary_to_term(bin, [:safe])}
  end

  @doc """
    Making a safe version we can match against, since we
    really don't want to risk killing a socket process over this.
  """
  def deserialize(bin) do
    try do
      {:ok, :erlang.binary_to_term(bin)}
    rescue
      ArgumentError -> {:error, :bad_args}
    end
  end
end
