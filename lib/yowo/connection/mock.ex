defmodule Yowo.Connection.Mock do
  @behaviour Yowo.Connection

  @moduledoc """
    A mock connection for testing.
  """

  @doc """
    This will relay the msg to the parent test,
    so long as we pretend it is the socket.
  """
  def send_contents(parent, contents) do
    send parent, contents
    :ok
  end

  def maintain_conn(mock) do
    :ok
  end

  def serialize(msg) do
    {:ok, msg}
  end

  def deserialize(msg) do
    {:ok, msg}
  end

  def deserialize!(msg) do
    {:ok, msg}
  end
end
