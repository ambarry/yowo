defmodule Yowo.Server.SocketController do
	alias :gen_tcp, as: GenTcp
	@callback start_socket_loop(Supervisor.supervisor, GenTcp.Socket) :: {:ok, pid} | {:error, term}
end
