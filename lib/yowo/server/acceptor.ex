defmodule Yowo.Server.Acceptor do
	require Logger
  alias :gen_tcp, as: GenTcp

  @doc """
    Accepts new socket connections on the passed in listen_socket.
    Passes off control to the handler module / sup.
  """
	def loop_acceptor(l_socket, socket_controller, control_sup) do
    res = case GenTcp.accept(l_socket) do
      {:ok, client} -> 
        Logger.debug "server: acceptor: new client socket"
        {:ok, pid} = socket_controller.start_socket_loop(control_sup, client)
        case GenTcp.controlling_process(client, pid) do
          :ok -> :ok # MUST transfer control!
          {:error, _reason} -> GenTcp.close(client)
        end 
      {:error, :timeout} ->
        Logger.debug "server: acceptor: connection timed out"
        :ok
      {:error, :closed} ->
        Logger.debug "server: acceptor: listen socket closed" # sup's job will be to restart all acceptors
        :stop 
      {:error, :system_limit} ->
        Logger.info "server: acceptor: reached system_limit, all ports in use"
        :timer.sleep(100)
        :ok
    end
    case res do
      :ok -> loop_acceptor(l_socket, socket_controller, control_sup)
      :stop -> :ok # what do we actually want to do here?  will this stop parent? need to signal?
    end
  end

  @doc """
    Creates dynamic acceptor name for Task pooling.
  """
  def get_acceptor_name(index) do
    Module.concat(["Yowo.Server.Accpetor_#{index}"])
  end
end