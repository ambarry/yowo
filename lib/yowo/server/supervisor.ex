defmodule Yowo.Server.Supervisor do
	use Supervisor
	require Logger
	alias :gen_tcp, as: GenTcp

  @moduledoc """
    Listens on port and spawns socket acceptors.
    Passes through handler module / sup.
  """

	def start_link(name, port, num_acceptors, socket_controller, control_sup) do
		Supervisor.start_link(__MODULE__, 
      {:ok, port, num_acceptors, socket_controller, control_sup}, [name: name])
	end

	def init({:ok, port, num_acceptors, socket_controller, control_sup}) do
		opts = [:binary, packet: 4, active: :once, reuseaddr: true]
    # TODO: backlog settings?
    # TODO: multiple ports? or each in diff sup? maybe port range, accpt per?
    # ranch recommends 100 acceptors
    case GenTcp.listen(port, opts) do
      {:ok, l_socket} ->
        Logger.info "server: listener: accepting connections on port #{port}..."
      	# args for the loop_acceptor task:
        loop_args = [l_socket, socket_controller, control_sup]
        # worker / task args: module, fun, args:
        worker_args = [Yowo.Server.Acceptor, :loop_acceptor, loop_args] 

        children = for i <- 0..(num_acceptors - 1) do
          w_id = Yowo.Server.Acceptor.get_acceptor_name(i)
          worker(Task, worker_args, [id: w_id])
        end
        supervise(children, strategy: :one_for_one)
      {:error, reason} -> exit(reason) # fail miserably
    end
	end
end