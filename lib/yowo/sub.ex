defmodule Yowo.Sub do
  use GenServer
  alias Yowo.{Command, Dispatcher, Hap, StageResult, User}
  require Logger

  @moduledoc """
    Yowo.Sub uses a Gen Server to maintain a basic subscriber:
    a socket and a user struct.
  """

  @connection Application.get_env(:yowo, :connection)[:mod]
  @pubsub Application.get_env(:yowo, :pubsub)[:mod]

  def start_link(socket, opts \\ []) do
    Logger.debug "sub GenServer: start_link"
    GenServer.start_link(__MODULE__, {:ok, socket}, opts)
  end

  def stop(pid) do
    GenServer.stop(pid)
  end

  # ---

  def init({:ok, socket}) do
    # TODO: make sure this is acceptable place to do this...
    # send :READY_FOR_AUTH
    Logger.debug "sub GenServer: init: sending READY_FOR_AUTH"
    stage = %StageResult{:stage => :READY_FOR_AUTH}
    @connection.send_contents(socket, stage)
    {:ok, %{
      :socket => socket,
      :user => nil,
      :is_authenticated => false,
      :current_location => nil}}
  end

  def handle_info({:broadcast, hap}, state) do
    Logger.debug "sub GenServer: user_id #{state.user.user_id}: handling (info) :broadcast, writing to socket..."

    # TODO: REMOVE
    IO.inspect hap

    cond do
      !Hap.is_active?(hap) -> {:noreply, state}
      User.blocks?(state.user, hap.user_id) -> {:noreply, state}
      true -> 
        case User.put_hap(state.user, hap) do
          {:ok, user} ->
            Logger.debug "sub GenServer: put haps through User, about to send the broadcasted hap"
            @connection.send_contents(state.socket, hap)
            Logger.debug "sub GenServer: hap sent to user"
            {:noreply, %{state | :user => user}}
          :error -> {:noreply, state}
        end    
    end
  end

  def handle_info({:tcp, _s, contents}, state) do
    Logger.debug "sub GenServer: handling (info) :tcp, processing command..."
    @connection.maintain_conn(state.socket)
    msg = @connection.deserialize(contents)
    # TODO: send back an unauthroized message
    # on nil user or unauth + :auth lvl cmd
    case Command.decode_command(msg) do
      {:ok, {cmd, auth_lvl}, data} ->
        Logger.debug "Got command #{cmd}"
        cond do
          (auth_lvl == :open) -> {:noreply, process_command(cmd, data, state)}
          state.user == nil -> {:noreply, state |> send_error(:not_authenticated)}
          state.is_authenticated -> {:noreply, process_command(cmd, data, state)}
          (auth_lvl != :open) -> {:noreply, state |> send_error(:not_authenticated)}
        end
      {:error, _reason} -> 
        Logger.debug "Couldn't decode a valid command"
        {:noreply, state}
    end
  end

  def handle_info({:tcp_closed, _s}, state) do
    Logger.debug "sub GenServer: handling (info) :tcp_closed, stopping server..."
    {:stop, :normal, state}
  end

  def handle_info({:tcp_error, _s, _reason}, state) do
    Logger.debug "sub GenServer: handling (info) :tcp_error, stopping server..."
    {:stop, :normal, state}
  end

  def handle_info(_, state) do
    {:noreply, state}
  end

  defp process_command(:register, %{:username => username, :email => email, 
    :secret => secret}, state) do
    Logger.debug "sub GenServier: registering new user #{username}"
    case User.create(username, email, secret) do
      {:ok, user} ->  
        send_login_result(state.socket, user)
        %{state | :user => user, :is_authenticated => true}
      {:error, reason} -> state |> send_error(:register)
    end
  end

  defp process_command(:authenticate, %{:username => username, :secret => secret}, state) do
    # TODO: token mode? Possibly a different command...
    # Authenticate the user, pull any needed info, and dispatch the original topic.
    # If auth fails, stop. otherwise, add users, etc.
    Logger.debug "sub GenServer: authenticating #{username}..."
    case User.authenticate(username, secret) do
      {:ok, user} -> 
        Logger.debug "sub GenServer: authenticated: sending login result"
        send_login_result(state.socket, user)
        %{state | :user => user, :is_authenticated => true}
      {:error, :bad_auth} -> state |> send_error(:authentication)
    end
  end

  defp process_command(:init_user, :ok, state) do
    # client knows it is authenticated and now can recv events
    Logger.debug "sub GenServer: user_id #{state.user.user_id}: initializing... "
    tops = Enum.concat(state.user.locations, state.user.follows)
    tops 
    |> Enum.each(fn top -> @pubsub.subscribe(top, self()) end)
    {:ok, user} = 
      state.user
      |> get_new_haps(tops)
      |> send_new_haps(state.socket)
      |> add_haps_to_user(state.user)
    # TODO: send the locations and followings in data
    stage = %StageResult{:stage => :INIT_COMPLETE}
    @connection.send_contents(state.socket, stage)
    Map.put(state, :user, user)
  end

  defp process_command(:arrive, %{:location => location}, state) do
    Logger.debug "sub GenServer: user_id: #{state.user.user_id}: arriving at new location..."
    # TODO: need to round location!!!
    rounded = Hap.round_location(location)
    # TODO: REMOVE THIS
    IO.inspect rounded

    has_new = User.has_location?(state.user, rounded)
    has_old = User.has_location?(state.user, state.current_location)
    pid = self()
    cond do
      state.current_location == rounded -> state
      has_new && has_old -> set_current_loc(state, rounded)
      has_new ->
        state
        |> unsub_from_current(pid)
        |> set_current_loc(rounded)
      has_old ->
        state
        |> sub_to_new_topic(rounded, pid)
        |> set_current_loc(rounded)
      true ->
        state
        |> unsub_from_current(pid)
        |> sub_to_new_topic(rounded, pid)
        |> set_current_loc(rounded)
    end
  end

  defp process_command(:save_location, %{:location => location}, state) do
    Logger.debug "sub GenServer: user_id: #{state.user.user_id}: saving new location..."
    case User.put_location(state.user, location) do
      {:ok, user} ->
        cond do
          location == state.current_location -> state |> set_user(user)
          true ->
            state
            |> set_user(user)
            |> sub_to_new_topic(location, self())
            |> send_ok(:save_location)
        end
      :error -> state |> send_error(:save_location)
    end
  end

  defp process_command(:remove_location, %{:location => location}, state) do
    Logger.debug "sub GenServer: user_id: #{state.user.user_id}: removing location..."
    case User.remove_location(state.user, location) do
      {:ok, user} ->
        unless current_location?(state, location), do: @pubsub.unsubscribe(location, self())
        state
        |> set_user(user)
        |> send_ok(:remove_location)
      :error -> state |> send_error(:remove_location)
    end
  end

  defp process_command(:follow, %{:follow_id => follow_id, :follow_name => follow_name}, state) do
    Logger.debug "sub GenServer: user_id: #{state.user.user_id}: following #{follow_id}..."
    case User.follow(state.user, follow_id, follow_name) do
      {:ok, user} ->
        state
        |> set_user(user)
        |> sub_to_new_topic(follow_id, self())
        |> send_ok(:follow)
      :error -> state |> send_error(:follow)
    end
  end

  defp process_command(:unfollow, %{:follow_id => follow_id, :follow_name => follow_name}, state) do
    Logger.debug "sub GenServer: user_id: #{state.user.user_id}: unfollowing #{follow_id}..."
    # async now -> won't receive error, need to wait to get it later...
    case User.unfollow(state.user, follow_id, follow_name) do
      {:ok, user} ->
        :ok = @pubsub.unsubscribe(follow_id, self)
        state
        |> set_user(user)
        |> send_ok(:unfollow)
      :error -> state |> send_error(:unfollow)
    end
  end

  defp process_command(:publish_hap, map, state) do
    # TODO: check publish frequency / last publish date at all?
    Logger.debug "sub GenServer: user_id: #{state.user.user_id}: publishing new hap..."
    res = with {:ok, hap} <- Hap.init_new_hap(map),
      {:ok, hap} <- set_hap_user(hap, state.user),
      {:ok, topics} <- Hap.generate_topics(hap),
      :ok <- Hap.put_hap(hap, topics) do
        Logger.debug "about to send hap to dispathcer for broadcast"
        # TODO: REMOVE THIS 
        IO.inspect(hap)
        topics |> Enum.each (fn t -> IO.inspect t end)

        msg = {:broadcast, hap}
        Dispatcher.multi_async(@pubsub, :broadcast, [topics, msg])
      end
    case res do
      :ok -> state |> send_ok(:publish_hap)
      _ -> state |> send_error(:publish_hap)
    end
  end

  # --- some useful combinator-y things, hap-related and other functions
  # TODO: improve, can we chain funcs better?

  defp send_ok(state, command) do
    @connection.send_contents(state.socket, %{:ok => command})
    state
  end

  defp send_error(state, command) do
    Logger.debug "sub GenServer: sending message through connection..."
    @connection.send_contents(state.socket, %{:error => command})
    state
  end

  defp current_location?(state, loc) do
    state.current_location == loc
  end

  defp set_hap_user(hap, user) do
    {:ok, %{hap | :user_id => user.user_id, :username => user.username}}
  end

  defp set_current_loc(state, new_loc) do
    %{state | :current_location => new_loc}
  end

  defp set_user(state, user) do
    %{state | :user => user}
  end

  defp unsub_from_current(state, pid) do
    :ok =
      cond do
        state.current_location == nil -> :ok
        true -> @pubsub.unsubscribe(state.current_location, pid)
      end
    state
  end

  defp sub_to_new_topic(state, topic, pid) do
    {:ok, ^pid} = @pubsub.subscribe(topic, pid)
    {:ok, user} =
      state.user
      |> get_new_haps(topic)
      |> send_new_haps(state.socket)
      |> add_haps_to_user(state.user)
    %{state | :user => user}
  end

  defp get_new_haps(user, topics) when is_list(topics) do
    topics 
    |> Hap.get_active_haps()
    |> Enum.filter(fn h -> !User.has_hap?(user, h) end)
  end

  defp get_new_haps(user, topic) do
    get_new_haps(user, [topic])
  end

  defp send_new_haps(haps, socket) do
    # TODO: bulk / list version!
    haps
    |> Enum.each(fn h -> 
      # TODO: REMOVE
      IO.inspect(h)
      @connection.send_contents(socket, h) 
    end)
    haps
  end

  defp add_haps_to_user(haps, user) do
    User.put_haps(user, haps)
  end

  defp send_login_result(socket, user) do
    res = %StageResult{
      :stage => :AUTH_COMPLETE, 
      :data => %{:user_id => user.user_id, :username => user.username}
    }
    @connection.send_contents(socket, res)
  end
end
