defmodule Yowo.StageResult do
	defstruct [
		stage: nil,
		data: %{}
	]
end