defmodule Yowo.TotalRecall do
	alias :mnesia, as: Mnesia

	@moduledoc """
		Mnesia helpers that aren't table specific.
		Anything for getting info, wrapping activities or transactions,
		or adding frags or nodes should be found here.
		Assumes table fragging.
	"""

	def act(f, args \\ []) do
    :async_dirty |> activity(f, args)
  end

  def sync_act(f, args \\ []) do
    :sync_dirty |> activity(f, args)
  end

  def transaction(f, args \\ []) do
    :transaction |> activity(f, args)
  end

  defp activity(context, f, args) do
  	# TODO: catch :exit, {from, reason} if this is a problem...
    Mnesia.activity(context, f, args, :mnesia_frag)
  end
end