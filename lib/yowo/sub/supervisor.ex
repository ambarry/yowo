defmodule Yowo.Sub.Supervisor do
  @behaviour Yowo.Server.SocketController
  use Supervisor
  require Logger

  @moduledoc """
    The subscriber supervsior.
    Starts and maintains subscriber GenServers.
    Passes along the dispatcher named process.
  """

  def start_link(name) do
    Logger.info "About to start sub supervisor"
    Supervisor.start_link(__MODULE__, :ok, name: name)
  end

  def start_socket_loop(supervisor, socket) do
    Supervisor.start_child(supervisor, [socket, []])
  end

  @doc """
    NOTE: when you use the simple_one_for_one strategy, you
    can only use one child here, which servers as a template for
    the start_child method.
    Creation will actually be handled by server or registry.
  """
  def init(:ok) do
    children = [
      worker(Yowo.Sub, [], restart: :temporary)
    ]

    supervise(children, strategy: :simple_one_for_one)
  end
end
