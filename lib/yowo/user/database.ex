defmodule Yowo.User.Database do
  require Logger
	alias :mnesia, as: Mnesia
  alias Yowo.TotalRecall, as: Recall
  alias Yowo.Hap
  import Bowfish

	@moduledoc """
		A database module for Users.
		Handles all Mnesia calls.
	"""

  @nodes Application.get_env(:yowo, :nodes)
  @ram_copies 0 #NOTE: ram really means ram only, disc is both, disc_only means what it sounds like
  @disc_copies 1
	@opts Application.get_env(:yowo, :user)
  @user_tab @opts[:user_table]
  @auth_tab @opts[:auth_table]
  @follows_tab @opts[:follows_table]
  @blocks_tab @opts[:blocks_table]


  def tables, do: [@user_tab, @auth_tab, @follows_tab, @blocks_tab]

  def init_schema do
    create_users_tab()
    create_auth_tab()
    create_follows_tab()
    create_blocks_tab()
  end

  defp create_users_tab do
    Mnesia.create_table @user_tab, [
      access_mode: :read_write,
      type: :set,
      storage_properties: [
        ets: [:compressed],
        dets: [auto_save: 5000]
      ],
      # attributes: really want to just store the struct,
      # but need to guarantee some unique fields on creation
      attributes: [
        :user_id,
        :the_user
      ],
      frag_properties: [
        n_fragments: @opts[:frags],
        node_pool: @nodes,
        n_disc_copies: @disc_copies,
        n_ram_copies: @ram_copies
      ]
    ]
  end

  defp create_auth_tab do
    # this table serves as a quick lookup by username
    # for auth purposes, and simplifies retrieving what
    # we need from the actual user struct
    # TODO: consider a reverse (or real) index for email?
    # NOTE: fragmentation is NOT working with that fk setup...
    Mnesia.create_table @auth_tab, [
      access_mode: :read_write,
      type: :set,
      storage_properties: [
        ets: [:compressed],
        dets: [auto_save: 5000]
      ],
      attributes: [
        :username,
        :email,
        :user_id,
        :salt,
        :salted_hash
      ],
      frag_properties: [
        #foreign_key: {@user_tab, :user_id} # NOPE, better off indexing if you need to...
        n_fragments: @opts[:frags],
        node_pool: @nodes,
        n_disc_copies: @disc_copies,
        n_ram_copies: @ram_copies
      ]
    ]
  end

  defp create_follows_tab do
    Mnesia.create_table @follows_tab, [
      access_mode: :read_write,
      type: :bag,
      storage_properties: [
        ets: [:compressed],
        dets: [auto_save: 5000]
      ],
      attributes: [
        :user_id,
        :follows_id,
        :follows_name
      ],
      frag_properties: [
        n_fragments: @opts[:frags],
        node_pool: @nodes,
        n_disc_copies: @disc_copies,
        n_ram_copies: @ram_copies
      ]
    ]
  end

  defp create_blocks_tab do
    Mnesia.create_table @blocks_tab, [
      access_mode: :read_write,
      type: :bag,
      storage_properties: [
        ets: [:compressed],
        dets: [auto_save: 5000]
      ],
      attributes: [
        :user_id,
        :blocks_id,
        :blocks_name
      ],
      frag_properties: [
        n_fragments: @opts[:frags],
        node_pool: @nodes,
        n_disc_copies: @disc_copies,
        n_ram_copies: @ram_copies
      ]
    ]
  end

  # --- Mnesia access ---

  # DON'T USE IN PRODUCTION
  def lookup(id) do
    case fn -> Mnesia.dirty_read(@user_tab, id) end |> Recall.sync_act() do
      [record] -> {:ok, from_record(record)}
      _ -> :error
    end
  end

  def find_auth(username) do
    f = fn -> Mnesia.read(@auth_tab, username) end
    case f |> Recall.act() do
      [{@auth_tab, ^username, _email, id, salt, salted_hash}] -> {:ok, {id, salt, salted_hash}}
      _ -> :error
    end
  end

  def init_user(id) do
    f = fn ->
      case read_user(id) do
        :error -> {:error, :no_exists}
        user -> 
          active_haps = Hap.filter_haps(user.haps)
          follows =
            Mnesia.read(@follows_tab, id)
            |> to_id_name()
            |> Map.new()
          blocks =
            Mnesia.read(@blocks_tab, id)
            |> to_id_name()
            |> Map.new()
          {:ok, %{user | :haps => active_haps, :follows => follows, :blocks => blocks}}
      end
    end
    f |> Recall.act()
  end

  defp to_id_name(records) do
    records
    |> Enum.map(fn {_tab, _user_id, id, name} -> {id, name} end)
  end

  defp username_available?(username) do
    Logger.debug "checking username"
    case fn -> Mnesia.read(@auth_tab, username) end |> Recall.sync_act() do
      [] -> true
      _ -> false
    end
  end

  defp email_available?(email) do
    # TODO: this is potentially expensive!
    case fn -> 
      Mnesia.match_object(@auth_tab, {@auth_tab, :"_", email, :"_", :"_", :"_"}, :read) 
    end
    |> Recall.sync_act() do
      [] -> true
      _ -> false
    end
  end

  defp to_record(user) do
    {@user_tab, user.user_id, user}
  end

  defp from_record([record]) do
    from_record(record)
  end

  defp from_record({@user_tab, _id, user}) do
    user
  end

  defp read_user(id) do
    case Mnesia.read(@user_tab, id) do
      [] -> :error
      u -> u |> from_record()
    end
  end

  def create_user(user, salt, salted_hash) do
    f = fn ->
      name_avail = username_available?(user.username)
      email_avail = email_available?(user.email)
      cond do
        name_avail && email_avail ->
          # save user`
          user
          |> to_record()
          |> Mnesia.write()

          # save auth lookup
          {@auth_tab, user.username, user.email, user.user_id, salt, salted_hash}
          |> Mnesia.write()
        name_avail -> {:error, :email_taken}
        email_avail -> {:error, :username_taken}
        true -> {:error, :username_and_email_taken}
      end
    end
    f |> Recall.act()
  end

  def consolidate_haps(new_haps, hap_map) do
    new_haps
    |> Enum.reduce(hap_map, fn (h, map) -> Map.put(map, h.hap_id, h.date_expires) end)
  end

  def write_hap(user_id, hap) do
    f = fn ->
      case read_user(user_id) do
        :error -> {:error, :no_exists}
        user ->
          # filter active now
          user.haps
          |> Hap.filter_haps()
          |> Map.put(hap.hap_id, hap.date_expires)
          >>> Map.put(user, :haps, :_)
          |> to_record()
          |> Mnesia.write()          
      end
    end
    f |> Recall.act()
  end

  def write_haps(user_id, haps) do
    f = fn ->
      case read_user(user_id) do
        :error -> {:error, :no_exists}
        user ->
          haps
          |> consolidate_haps(user.haps)
          |> Hap.filter_haps() # filter active now
          >>> Map.put(user, :haps, :_)
          |> to_record()
          |> Mnesia.write()
      end
    end
    f |> Recall.act()
  end

  def write_follow(user_id, follow_id, follow_name) do
    write_simple_assoc(@follows_tab, user_id, follow_id, follow_name)
  end

  def delete_follow(user_id, follow_id, follow_name) do
    delete_simple_assoc(@follows_tab, user_id, follow_id, follow_name)
  end

  def write_block(user_id, block_id, block_name) do
    write_simple_assoc(@blocks_tab, user_id, block_id, block_name)
  end

  def delete_block(user_id, block_id, block_name) do
    delete_simple_assoc(@blocks_tab, user_id, block_id, block_name)
  end

  defp write_simple_assoc(table, user_id, assoc_id, name) do
    f = fn ->
      case Mnesia.read(@user_tab, assoc_id) do
        [] -> {:error, :no_exists}
        _ ->
          {table, user_id, assoc_id, name}
          |> Mnesia.write()
      end
    end
    f |> Recall.act()
  end

  defp delete_simple_assoc(table, user_id, id, name) do
    f = fn ->
      {table, user_id, id, name}
      |> Mnesia.delete_object()
    end
    f |> Recall.act()
  end

  def write_location(user_id, location) do
    f = fn ->
      case read_user(user_id) do
        :error -> {:error, :no_exists}
        user ->
          user
          |> Map.put(:locations, MapSet.put(user.locations, location))
          |> to_record()
          |> Mnesia.write()
          :ok
      end
    end
    f |> Recall.act()
  end

  def delete_location(user_id, location) do
    f = fn ->
      case read_user(user_id)do
        :error -> {:error, :no_exists}
        user ->
          user
          |> Map.put(:locations, MapSet.delete(user.locations, location))
          |> to_record()
          |> Mnesia.write()
          :ok
      end
    end
    f |> Recall.act()
  end

  def delete_user(id) do
    f = fn ->
      Mnesia.delete({@user_tab, id})
      Mnesia.delete({@follows_tab, id})
      Mnesia.delete({@blocks_tab, id})
      delete_all_associated_with(@follows_tab, id)
      delete_all_associated_with(@blocks_tab, id)
      :ok
    end
    f |> Recall.act()
  end

  defp delete_all_associated_with(table, id) do
    {table, :"_", id, :"_"}
    |> Mnesia.match_object()
    |> Enum.each(fn r -> Mnesia.delete_object(r) end)
  end
end