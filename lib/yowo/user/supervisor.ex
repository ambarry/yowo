defmodule Yowo.User.Supervisor do
	use Supervisor
	alias Yowo.User

	@opts Application.get_env(:yowo, :user)
  @pool_size @opts[:pool_size]

  def start_link(name) do
  	Supervisor.start_link(__MODULE__, :ok, [name: name])
  end

  def init(:ok) do
  	children = for i <- 0..(@pool_size - 1) do
  		worker_name = User.get_server_name(i)
  		worker(User, [worker_name], [id: worker_name])
  	end
  	supervise(children, strategy: :one_for_one)
  end
end