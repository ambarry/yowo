defmodule Yowo.PubSub.Server do
  @behaviour Yowo.PubSub
  use GenServer
  require Logger
  alias Yowo.Pooler

  @moduledoc """
    Handles subscriptions to topics.
    Broadcasts messages to subscribers.
    Meant to be pooled.
  """

  @opts Application.get_env(:yowo, :pubsub)
  @pool_size @opts[:pool_size]


  def start_link(name, sub_topics) do
    Logger.info "starting PubSub Server: #{name}..."
    GenServer.start_link(__MODULE__, {:ok, sub_topics}, [name: name])
  end

  def broadcast(topics, msg) when is_list(topics) do
    Logger.debug "PubSub broadcasting"
    topics
    |> Enum.each(fn t -> 
      spawn fn ->
        broadcast(t, msg)
      end
    end)
    :ok
  end

  def broadcast(topic, msg) do
    topic
    |> get_subs_by_topic()
    |> Enum.each(fn {_topic, sub} -> send sub, msg end)
    :ok
  end

  def subscribe(topic, sub) do
    sub
    |> get_pubsub_server()
    |> GenServer.call({:put_sub, topic, sub})
  end

  def unsubscribe(topic, sub) do
    # TODO: does this need to be a call?? doesn't seem like it yet...
    true = delete_topic_sub(topic, sub)
    true = delete_sub_topic(sub, topic)
    :ok
  end

  def stop(pubsub) do
    GenServer.stop(pubsub, :stop)
  end

  defp get_subs_by_topic(topic) do
    topic
    |> get_topic_table()
    |> :ets.lookup(topic)
  end

  defp get_topics_by_sub(table, sub) do
    :ets.lookup(table, sub)
  end

  defp delete_topic_sub(topic, sub) do
    topic
    |> get_topic_table()
    |> :ets.delete_object({topic, sub})
  end

  defp delete_sub_topic(sub, topic) do
    sub
    |> get_sub_table()
    |> :ets.delete_object({sub, topic})
  end

  def get_topic_table_name(index), do: Pooler.get_table_name("yowo_pubsub_topic_subs", index)
  def get_sub_table_name(index), do: Pooler.get_table_name("yowo_pubsub_sub_topics", index)
  def get_pubsub_server_name(index), do: Pooler.get_server_name("Yowo.PubSub.Server", index)
  defp get_topic_table(topic), do: Pooler.resolve(topic, @pool_size, &get_topic_table_name/1)
  defp get_sub_table(sub), do: Pooler.resolve(sub, @pool_size, &get_sub_table_name/1)
  defp get_pubsub_server(sub), do: Pooler.resolve(sub, @pool_size, &get_pubsub_server_name/1)
  
  # ---

  def init({:ok, sub_topics}) do
    # TODO: this seems expensive, even if it should be one-time / rare...
    # can we optimize this? is there a way to get distinct keys from ets directly?
    # or is it worth a delve into process groups?
    # does this risk a leak if any go down while this is starting?
    # worth checking out how others are handling this
    refs = :ets.foldl(&remonitor(&1, &2), Map.new(), sub_topics)
    {:ok, %{:refs => refs, :sub_topics => sub_topics}}
  end

  defp remonitor({sub, _topic}, acc) do
    unless Map.has_key?(acc, sub) do
      acc |> Map.put(sub, Process.monitor(sub))
    else
      acc
    end
  end

  def handle_call({:put_sub, topic, sub}, _from, state) do
    true =
      topic
      |> get_topic_table()
      |> :ets.insert({topic, sub})
    true = :ets.insert(state.sub_topics, {sub, topic})
    ref = Process.monitor(sub)
    {:reply, {:ok, sub}, %{state | :refs => Map.put(state.refs, sub, ref)}}
  end

  def handle_call(:stop, _from, state) do
		{:stop, :normal, :ok, state}
	end

  def handle_info({:DOWN, _ref, :process, pid, _reason}, state) do
    case Map.pop(state.refs, pid) do
      {nil, _refs} ->
        {:noreply, state}
      {ref, refs} ->
          Process.demonitor(ref)
          get_topics_by_sub(state.sub_topics, pid)
          |> Enum.each(fn {sub, topic} -> delete_topic_sub(topic, sub) end)
          :ets.delete(state.sub_topics, pid) # will delete all objs w/ key
          {:noreply, %{state | :refs => refs}}
    end
	end

	def handle_info(_msg, state) do
		{:noreply, state}
	end
end
