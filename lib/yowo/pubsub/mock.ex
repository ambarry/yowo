defmodule Yowo.PubSub.Mock do
  @behaviour Yowo.PubSub

  @moduledoc """
    A hacky pseudo-GenServer for testing.
    Basically just relays the messages that the
    PubSub server would've received back to the test
    process.
  """

  @mock :mock_pub_sub

  @doc """
    Hijacking start link -> you've been warned!!!
  """
  def start_link(:init, _test_case) do
    pid = spawn fn ->
      relay(Map.new())
    end
    Process.register(pid, @mock)
    {:ok, pid}
  end

  def start_link(:add_test, {test, sub}) do
    send @mock, {:add_test, test, sub}
    :ok
  end

  def start_link(:add_broadcast_test, {test_name, node, key}) do
    send @mock, {:add_broadcast_test, test_name, node, key}
    :ok
  end

  def broadcast(topics, msg) do
    # NOTE: self is NOT the sub when sent through task! this is no longer valid!
    IO.puts "broadcasting..."
    send @mock, {:send_it, :bcast, {:broadcast, topics, msg}}
    :ok
  end

  def subscribe(topic, sub) do
    send @mock, {:send_it, sub, {:sub, topic, sub}}
    {:ok, sub}
  end

  def unsubscribe(topic, sub) do
    send @mock, {:send_it, sub, {:unsub, topic, sub}}
    :ok
  end

  defp relay(sub_tests) do
    receive do
      {:add_test, test, sub} ->
        sub_tests
        |> Map.put(sub, test)
        |> relay()
      {:send_it, sub, msg} ->
        test = Map.get(sub_tests, sub)
        if sub == :bcast do
          {tn, node} = test
        end
        send test, msg
        relay(sub_tests)
      {:add_broadcast_test, test_name, node, key} ->
        sub_tests
        |> Map.put(key, {test_name, node})
        |> relay()
    end
  end
end
