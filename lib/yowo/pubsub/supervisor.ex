defmodule Yowo.PubSub.Supervisor do
  use Supervisor
  alias Yowo.PubSub.Server, as: PubSub

  @opts Application.get_env(:yowo, :pubsub)
  @pool_size @opts[:pool_size]

  def start_link(name) do
    Supervisor.start_link(__MODULE__, :ok, [name: name])
  end

  def init(:ok) do
    opts = [:duplicate_bag, :named_table, :public, read_concurrency: true, write_concurrency: false]
    children = for i <- 0..(@pool_size - 1) do
      # start ets tables
      _topic_subs = i
        |> PubSub.get_topic_table_name()
        |> :ets.new(opts)
      sub_topics = i
        |> PubSub.get_sub_table_name()
        |> :ets.new(opts)
      worker_name = PubSub.get_pubsub_server_name(i)
      # return worker w/ args
      worker(PubSub, [worker_name, sub_topics], [id: worker_name])
    end
    supervise(children, strategy: :one_for_one)
  end
end
