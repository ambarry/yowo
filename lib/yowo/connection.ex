defmodule Yowo.Connection do
  @type data :: %{}
  @type reason :: atom | :inet.posix

  @callback send_contents(any, any) :: :ok | {:error, reason}
  @callback maintain_conn(any) :: :ok | {:error, reason}
  @callback serialize(any) :: {:ok, binary}
  @callback deserialize(binary) :: {:ok, any} | {:error, :bad_args}
  @callback deserialize!(binary) :: {:ok, any}
end
