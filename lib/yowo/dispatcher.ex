defmodule Yowo.Dispatcher do
	require Logger

	@moduledoc """
		Yowo.Dispatcher
		Dispatches a function to a specific node based on hashing an id.
		Dispatches funcs asynchronously to each node in pool.
		Uses supervised async tasks to send a message per node.
		NOTE: this module assumes we have a Task supervisor already running.
		Nodes are retrieved from ets for dynamic config.
	"""

	@nodes Application.get_env(:yowo, :nodes)
	@node_table :yowo_dispatcher_nodes_tab
	@node_row :node_row_key

	@doc """
		Stores initial nodes in ets table for retrieving.
		Storing in one row only, since all are accessed at once, 
		and we want the list to be concurrent / safe?
	"""
	def init(nodes) do
		# TODO:
		:ets.new(@node_table, [:named_table, read_concurrency: true])
	end


	@doc """
		Runs a function asynchronously on the
		node derived through hashing an id.
	"""
	def async(id, mod, fun, args) do
		id
		|> get_node()
		|> start_task(mod, fun, args)
	end

	@doc """
		Runs a function w/ args from a specified module
		asynchronously across all nodes.
	"""
	def multi_async(mod, fun, args) do
		Logger.debug "Dispatcher about to async dispatch..."
		@nodes |> Enum.each(&(start_task(&1, mod, fun, args)))
	end

	@doc """
		A test helper. Do not use in production.
	"""
	def report_hello(dest) do
		n = node()
		Process.send(dest, "Hello from #{n}", [])
	end

	defp get_node(id) do
		idx = :erlang.phash2(id, Enum.count(@nodes))
		Enum.at(@nodes, idx)
	end

	defp get_node_list() do
		# TODO:
	end

	defp start_task(node, mod, fun, args) do
		Logger.debug "starting task from Dispatcher!!!"
		{Yowo.DispatcherTasks, node}
		|> Task.Supervisor.async(mod, fun, args)
	end
end