defmodule Yowo.Command do
  alias Yowo.Packet

  @moduledoc """
    Defines basic commands for Yowo.
  """

  @doc """
    Matches strings with Yowo command atoms.
  """
  @spec parse_command(String.t) :: {atom, atom}
  def parse_command(cmd) do
    # TODO: register new user? (can still be a cmd even if outside socket... (or stay in!))
    case cmd do
      "AUTHENTICATE" -> {:authenticate, :open}
      "ARRIVE" -> {:arrive, :auth}
      "BLOCK_USER" -> {:block_user, :auth}
      "FLAG" -> {:flag, :auth}
      "FOLLOW" -> {:follow, :auth}
      "INIT_USER" -> {:init_user, :auth}
      "LIKE" -> {:like, :auth}
      "PUBLISH_HAP" -> {:publish_hap, :auth}
      "REGISTER" -> {:register, :open}
      "REMOVE_LOCATION" -> {:remove_location, :auth}
      "SAVE_LOCATION" -> {:save_location, :auth}
      "UNFOLLOW" -> {:unfollow, :auth}
      _ -> {:unknown_command, :auth}
    end
  end

  @doc """
    Parses msg into command atom.
  """
  def decode_command(msg) do
    case msg do
      {:ok, %Packet{command: cmd, data: data}} -> {:ok, parse_command(cmd), data}
      _ -> {:error, :bad_msg}
    end
  end
end
