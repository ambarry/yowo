defmodule Yowo.Hap.Database do
	alias :mnesia, as: Mnesia
  alias :yowo_hap_query, as: HapQ
  alias Yowo.TotalRecall, as: Recall

  @nodes Application.get_env(:yowo, :nodes)
  @ram_copies 0
  @disc_copies 1
	@opts Application.get_env(:yowo, :hap)
  @hap_tab @opts[:hap_table]
  @topic_tab @opts[:topic_table]


  def tables do
    [@hap_tab, @topic_tab]
  end

  # TODO: clean topic haps

  def init_schema do
    create_haps_table()
    create_topic_table()
  end

  defp create_haps_table() do
    Mnesia.create_table @hap_tab, [
      access_mode: :read_write,
      type: :set,
      storage_properties: [
        ets: [:compressed],
        dets: [auto_save: 5000]
      ],
      attributes: [
        :hap_id,
        :the_hap
      ],
      frag_properties: [
        n_fragments: @opts[:frags],
        node_pool: @nodes,
        n_disc_copies: @disc_copies,
        n_ram_copies: @ram_copies
      ]
    ]    
  end

  defp create_topic_table() do
    Mnesia.create_table @topic_tab, [
      access_mode: :read_write,
      type: :bag, # location table is a bag -> many to many
      storage_properties: [
        ets: [:compressed],
        dets: [auto_save: 5000]
      ],
      attributes: [
        :topic, # topic can be a location or  the tuple -> {:user_id, id}
        :hap_id,
        :date_expires, # NOTE: store this in gregorian seconds for easier matching!
      ],
      frag_properties: [
        n_fragments: @opts[:frags],
        node_pool: @nodes,
        n_disc_copies: @disc_copies,
        n_ram_copies: @ram_copies
      ]
    ]
  end

  
  def get_active_haps(topics) do
    now = Yowo.DateTime.now()
    f = fn ->
      topics
      |> HapQ.get_active_haps(now, @hap_tab, @topic_tab)
    end
    case f |> Recall.act() do
      res when is_list(res) -> res
      _ -> :error
    end
  end

  def write_hap(hap, topics) do
    fn ->
      Mnesia.write({@hap_tab, hap.hap_id, hap})
      topics
      |> Enum.each(fn t -> Mnesia.write({@topic_tab, t, hap.hap_id, hap.date_expires}) end)
      :ok
    end
    |> Recall.act()
  end

  # this one is much more complex but also more flexible
  # for instance, you could do modify to a dirty read to get hap_ids, then
  # pull haps (or call the qlc func)
  # can it be paged or made lazy w/ take? (qlc offers advantage here w/ cursor...)
  # but the pipes!!! :)
  # also, may want to bring back the select w/ matchhead and guard instead
  # of all ids and then filter on expired...
  # see end of module
  defp get_active_haps_alt(topics) do
    now = Yowo.DateTime.now()
    f = fn ->
      topics
      |> Enum.map(fn t ->
        @topic_tab
        |> Mnesia.read(t, :read)
        |> Enum.filter_map(&filter_expired(&1, now), &map_rec(&1))
      end)
      |> Enum.reduce(fn (x, acc) -> x ++ acc end)
      |> Enum.uniq()
      |> Enum.map(fn id -> Mnesia.read(@hap_tab, id) end)
      |> Enum.map(fn [{_tab, _id, hap}] -> hap end)
    end
    case f |> Mnesia.transaction() do
      {:atomic, res} -> {:ok, res}
      {:aborted, reason} -> {:error, reason}
    end
  end
  
  defp filter_expired({_tab, _top, _id, exp}, now), do: exp > now
  defp map_rec({_tab, _top, id, _exp}), do: id
end