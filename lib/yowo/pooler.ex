defmodule Yowo.Pooler do

	@moduledoc """
		Pooling module. Simple hashes and names.
	"""

	def resolve(t, pool_size, namer) do
		t
		|> :erlang.phash2(pool_size)
		|> namer.()
	end

	def get_table_name(prefix, index) do
		"#{prefix}_#{index}" |> String.to_atom()
	end

	def get_server_name(prefix, index) do
    # trick to create alias
    Module.concat(["#{prefix}_#{index}"])
  end
end