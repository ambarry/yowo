defmodule Yowo.PubSub do
  @callback start_link(any, any) :: {:ok, pid}
  @callback broadcast(any, any) :: :ok
  @callback subscribe(any, pid) :: {:ok, pid}
  @callback unsubscribe(any, pid) :: :ok
end
