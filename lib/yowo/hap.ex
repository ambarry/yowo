defmodule Yowo.Hap do
  use GenServer
  require Logger
  alias Yowo.{Dispatcher, Pooler}
  alias Yowo.Hap.Database, as: HapDb


  @moduledoc """
    Handles Hap / Event retrieval and writing.
    Writes go through GenServer, while reads hit
    Mnesia.
    GenServer should be pooled.
  """

  @opts Application.get_env(:yowo, :hap)
  @pool_size @opts[:pool_size]


  # TODO: counter for likes, if you go that way?
  # NOTE: don't generate the guid in struct default id, or it will be same for all!!!
  # TODO: categories e.g. :general, :free_stuff, :food, :bizarre, 
  #   :need_help, :fubar

  defstruct [
    user_id: "",
    username: "",
    title: "",
    message: "",
    location: {0.0, 0.0},
    hap_id: nil,
    date_post: nil,
    date_expires: nil,
    duration: 3600,
    category: :general
  ]


  # set up pooling.
  # again, don't worry about table names, as handling in Mnesia frags.


  def get_server_name(index), do: Pooler.get_server_name("Yowo.Hap.Server", index)
  def get_server(hap_id), do: Pooler.resolve(hap_id, @pool_size, &get_server_name/1)


  # public API ---

  def start_link(name) do
    Logger.info "hap GenServer: #{name}: start_link"
    GenServer.start(__MODULE__, :ok, [name: name])
  end

  def init(:ok) do
    {:ok, %{}} # no real state, just managing writes
  end

  def filter_haps(haps) do
    now = Yowo.DateTime.now()
    haps
    |> Enum.filter(fn {_k, v} -> v > now end)
    |> Map.new()
  end

  def is_active?(hap) do
    hap.date_expires > Yowo.DateTime.now()
  end

  def put_hap(hap, topics) do
    # TODO: consider going by topic, not hap? ehhh, it's a bag anyway,
    # and that would mean a lot of cross node messages here
    _task = Dispatcher.async(hap.hap_id, Yowo.Hap, :do_put_hap, [hap, topics])
    :ok
  end

  def do_put_hap(hap, topics) do
    hap.hap_id
    |> get_server()
    |> GenServer.cast({:put_hap, hap, topics})
  end

  def get_active_haps(topics) do
    HapDb.get_active_haps(topics)
  end

  @doc """
    Parses initial hap from map, returning :error if bad keys.
    Sets id and dates.
  """
  def init_new_hap(map) when is_map(map) do
    # TODO: validate title and msg and category (atom)
    try do
      hap =
        struct!(Yowo.Hap, map)
        |> set_initial_values()
      {:ok, hap}
    rescue
      _ex -> :error
    end
  end

  def parse_hap_with_expiry(_m) do
    :error
  end

  defp set_initial_values(hap) do
    now = Yowo.DateTime.now()
    %{hap |
      :hap_id => UUID.uuid4(),
      :date_post => now,
      :date_expires => now + hap.duration
    }
  end

  @doc """
    Generates a list of relevant topic
    s from the hap.
    Really, this means one for each nearby location,
    and one for the user who posted it.
  """
  def generate_topics(hap) do
    locs =
      hap.location
      |> round_location()
      |> get_boundaries()
      |> get_loc_list() # try to use the actual tuple for now...
      # |> Enum.map(&(location_to_atom(&1)))
    {:ok, [hap.user_id | locs]}
  end

  @doc """
    Rounds a location {lat, lng} to the
    nearest 5 thousandth (.005) in each.
  """
  def round_location({lat, lng}) do
    {round_float_to_five_thousandth(lat),
    round_float_to_five_thousandth(lng)}
  end

  @doc """
    Converts a location to an atom for easier matching.
  """
  def location_to_atom({lat, lng}) do
    "#{lat}_#{lng}"
    |> String.to_atom()
  end

  defp round_float_to_five_thousandth(f) do
    floor = f |> Float.floor(2)
    ceil = floor + 0.01
    mid = ceil - 0.005
    [floor, ceil, mid]
    |> Enum.min_by(fn x -> abs(x - f) end)
    |> Float.round(3)
  end

  defp get_loc_list({{lat, lat_min, lat_max}, {lng, lng_min, lng_max}}) do
    [
      {lat, lng},
      {lat, lng_max},
      {lat, lng_min},
      {lat_max, lng},
      {lat_max, lng_max},
      {lat_max, lng_min},
      {lat_min, lng},
      {lat_min, lng_max},
      {lat_min, lng_min}]
  end

  defp get_boundaries({lat, lng}) do
     {lat_min, lat_max} = get_round_min_max(lat, 0.005, 3)
     {lng_min, lng_max} = get_round_min_max(lng, 0.005, 3)
     {{lat, lat_min, lat_max}, {lng, lng_min, lng_max}}
  end

  defp get_round_min_max(f, amount, precision) do
    min = add_round(f, -amount, precision)
    max = add_round(f, amount, precision)
    {min, max}
  end

  defp add_round(f, adder, precision) do
    (f + adder)
    |> Float.round(precision)
  end


  # server callbacks ---
  
  def handle_cast({:put_hap, hap, topics}, state) do
    Logger.debug "hap GenServer handling (cast) :put_hap"
    :ok = HapDb.write_hap(hap, topics) # since this is now async, how do we handle error?
    {:noreply, state}
  end

  def handle_info(_, state)do
    Logger.debug "hap GenServer: UNKNOWN message"
    {:noreply, state}
  end
end
