defmodule Yowo.Client do
	use GenServer
	require Logger
	alias Yowo.Packet
	alias Yowo.Connection.Socket, as: Socket
	alias :gen_tcp, as: GenTcp

	@moduledoc """
  	This is a client for testing.
  """

  #@host {162, 243, 209, 109}
  #@port 4040
  #register "c0c07", "c0c0@yowo.com", "c0c08"

  def start_link(host, port, name) do
  	Logger.debug("client starting: name: #{name}: connecting to port #{port}")
  	opts = [:binary, packet: 4, active: :once]
  	GenServer.start_link(__MODULE__, {:ok, host, port, opts}, [name: name])
  end

  def register(server, username, email, secret) do
  	data = %{
  		:username => username,
  		:email => email,
  		:secret => secret
  	}
  	pak = %Packet{:command => "REGISTER", :data => data}
  	{:ok, msg} = Socket.serialize(pak)
  	GenServer.call(server, {:send_socket, msg})
  end

  def authenticate(server, username, secret) do
  	data = %{:username => username, :secret => secret}
  	pak = %Packet{:command => "AUTHENTICATE", :data => data}
  	{:ok, msg} = Socket.serialize(pak)
  	GenServer.call(server, {:send_socket, msg})
  end

  def arrive(server, location) do
    data = %{:location => location}
  	pak = %Packet{:command => "ARRIVE", :data => data}
  	{:ok, msg} = Socket.serialize(pak)
  	GenServer.call(server, {:send_socket, msg})
  end

  def init_user(server) do
    pak = %Packet{:command => "INIT_USER", :data => :ok}
    {:ok, msg} = Socket.serialize(pak)
    GenServer.call(server, {:send_socket, msg})
  end

  def save_location(server, location) do
    data = %{:location => location}
  	pak = %Packet{:command => "SAVE_LOCATION", :data => data}
  	{:ok, msg} = Socket.serialize(pak)
  	GenServer.call(server, {:send_socket, msg})
  end

  def remove_location(server, location) do
    data = %{:location => location}
  	pak = %Packet{:command => "REMOVE_LOCATION", :data => data}
  	{:ok, msg} = Socket.serialize(pak)
  	GenServer.call(server, {:send_socket, msg})
  end

  # ---

  def init({:ok, host, port, opts}) do
  	{:ok, socket} = GenTcp.connect(host, port, opts)
  	{:ok, %{:socket => socket}}
  end

  def handle_call({:send_socket, msg}, _from, state) do
  	res = GenTcp.send(state.socket, msg)
  	{:reply, res, state}
  end

  def handle_info({:tcp, _s, contents}, state) do
  	Logger.debug("client: received tcp msg. inspecting contents...")
  	Socket.maintain_conn(state.socket)
  	{:ok, msg} = Socket.deserialize(contents)
  	IO.inspect(msg)
  	{:noreply, state}
  end


  def handle_info(_msg, state) do
  	{:noreply, state}
  end

end