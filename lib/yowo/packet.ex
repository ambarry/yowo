defmodule Yowo.Packet do
  defstruct [
    command: "UNKNOWN",
    data: %{}
  ]
end
