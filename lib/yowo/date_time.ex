defmodule Yowo.DateTime do

  @moduledoc """
  Standard datetime logic to use across app, anticipating changes.
  Decprecating use of gregorian seconds in favor of 
  Elixir DateTime and Calendar modules as of Elixir 1.3.0 release.  
  """

  def now do
    DateTime.utc_now() |> DateTime.to_unix()
  end
end
