defmodule Yowo.Mixfile do
  use Mix.Project

  def project do
    [app: :yowo,
     version: "0.0.1",
     elixir: "~> 1.2",
     build_embedded: Mix.env == :prod,
     start_permanent: Mix.env == :prod,
     deps: deps]
  end

  def application do
    [
      applications: [:logger],
      mod: {Yowo, []}
    ]
  end

  defp deps do
    [
      {:bowfish, "~> 0.1.0"},
      {:uuid, "~> 1.1"}
    ]
  end
end
