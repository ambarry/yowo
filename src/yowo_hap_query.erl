-module(yowo_hap_query).
-export([get_haps/2, get_active_haps/4]).
-include_lib("stdlib/include/qlc.hrl").


% TODO: try to make sure these use key lookup,
% otherwise fetch the topics first from mnesia directly,
% then join

% TODO: consider using a cursor if this returns too many...

get_haps(Ids, HapTab) ->
  QH = qlc:q([Hap ||
    {_htab, HapId, Hap} <- mnesia:table(HapTab),
    Id <- Ids,
    HapId == Id
  ]),
  qlc:eval(QH).

get_active_haps(Topics, Now, HapTab, TopicTab) when is_list(Topics) ->
  QH = qlc:q([Hap ||
    {_htab, H_HapId, Hap} <- mnesia:table(HapTab),
    {_ttab, Top, T_HapId, Expires} <- mnesia:table(TopicTab),
    Topic <- Topics,
    H_HapId == T_HapId,
    Top == Topic,
    Expires > Now
  ], {unique, true}),
  qlc:eval(QH);
get_active_haps(Topic, Now, HapTab, TopicTab) ->
  QH = qlc:q([Hap ||
    {_htab, H_HapId, Hap} <- mnesia:table(HapTab),
    {_ttab, Top, T_HapId, Expires} <- mnesia:table(TopicTab),
    H_HapId == T_HapId,
    Top == Topic,
    Expires > Now
  ]),
  qlc:eval(QH).
