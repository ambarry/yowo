-module(yowo_query).
-export([test_query/1, test_contains/2, test_join/3, get_query_handle/1]).
-include_lib("stdlib/include/qlc.hrl").

test_query(T1) ->
  QH = qlc:q([X
      || X <- ets:table(T1)
    ]),
  qlc:eval(QH).

test_contains(T1, List) ->
  QH = qlc:q([{XId, Name} ||
    {XId, Name} <- ets:table(T1),
    YId <- List,
    XId == YId
  ]),
  qlc:eval(QH).

test_join(T1, T2, Now) ->
  QH = qlc:q([{XId, Name} ||
    {XId, Name} <- ets:table(T1),
    {YId, DateExpires} <- ets:table(T2),
    XId == YId,
    DateExpires > Now
  ]),
  qlc:eval(QH).

get_query_handle(T1) ->
  qlc:q([X ||
   X <- ets:table(T1)
  ]).
