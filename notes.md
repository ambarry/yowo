# Notes

## running detached
* elixir --detached -S mix run --no-halt
* need a release (through exrm?) when actually ready, code migrations, etc.
  - a release contains real runtime

## on the docket
[x] Return user info on Auth -> need a result wrapper for Protocol on client
[ ] Accept last connect time on init, only return haps that were posted after?
  [ ] as long as this is device specific, has potential value
[ ] Location -> db needs a name field too, even if you can't change...
  - decide on rounding strategy and validation
  - may want actual lat and lng for a marker display, and the rounded for the save and pubsub
[ ] client / app -> notification when in range of markers?
  - this would be cool, but not a necessity for mvp
[x] Ready / connected msg, ready for auth msg, ready for init, etc.
  * this way client knows the state of the connection and can proceed
  * standardize rather than atoms? eh...
[ ] Image handling?
  * need route, resize, way to store (fp or blob?), way to serve
  * maybe just have cowboy runnning as dep, for files
    * ORRRR, your own simple one? only those exts?
[ ] Dispatcher
  [ ] pull nodes from ETS (in-mem, should be okay?)
  [ ] make sure the ETS table is created on startup, read_concurrency
    * can call func in Dispatcher module
  [ ] function to add / remove nodes for ALL running nodes (minus the removed one I guess)
[ ] Mnesia
  [ ] wrapper to add / remove frags
  [ ] wrapper to add / remove nodes
[ ] make sure block also unfollows and unsubscribes, if necessary...
[ ] Better logging and responses so we can debug!



## error handling [sub / mnesia write servers]
[ ] adjust genserver for error handling (User esp, maybe Hap too, and end in Sub)
  * async just means the replication, but STILL RETURNS
    - the genServer still gets the reply!
    - so, include pid w/ msg?
    - function to send success or error on sub:
      + accept sub -> server can be {atom, node} (or pid, but across nodes needs registering?)
      + not sure what to do there...
      + it will do a genserver cast to the sub 
      + handle there!
    - alt, could change to call, send noreply, then genserver.reply()
    - or, could have a pooled error relay server, use the node, pid for that...
    - **or just distributed task that runs func on other node, uses pid from msg...**
  * can use this to know to send back a msg, handle in sub
  * may have to pass sub pid through...
  * should match on new user returns, e.g. {:error, :no_exists}

## appraisal system
[ ] fair vs foul, dark souls style
[ ] display ct only, update the record
  * do we want to push on ct? could be cool, but a lot of overhead...
  * or, push on some sort of refresh / re-init only
  * or, just update on the init on client -> if dup, update counter only
  * this way, no additional backend overhead 
    * at least, if we pull from hapDb, not user on startup
    * need to check if this is normalized
    * if not, do it!

## pool server [server / acceptor / yowo supervisor]
[x] actual pooling and supervisor
[x] test multiple acceptors
[ ] test crash handling
[ ] multiple ports? (have each listen on a different port)
[ ] pull port range, num acceptors from config
  * choose some rare ones, not 4040 which is in Elixir demos...
[ ] separate project under umbrella, if you can
  - although, needs sub supervisor so...
    + but if this is named, you're okay
  - really want, since may end up on separate nodes ultimately

## distributed architecture improvement
[ ] should really split up front-end (server, sub, pubsub) vs back-end (logic, user, hap) vs service (mnesia and db modules)
  * at least front-end and back-end
  * should make scaling each part easier
  * would only have to replicate data across the mnesia nodes then, still hash to write, read from any
  * front-end and back-end are totally independent and can have any number of nodes
[ ] Consider converting pubsub to pg2? not sure how the hashing  or monitoring would work though...
[ ] self-discovering nodes? heartbeat / pulse rather than manually maintain ets table?
  * becomes a heartbeat process + ...
  * don't necessarily want agent, as that would be a bottleneck on all broadcasts

## tests
[ ] init user tests
[ ] full integration test w/ socket client, multi-nodes, go through socket, not test pid
  * http://elixir-lang.org/getting-started/mix-otp/docs-tests-and-with.html#running-commands
[ ] improve tests
  * sub and user really need to be cleaned up for multi-node and fragmentation
    - too many dirty writes right now
    - redo setup_all and setup to make sense, then go from there
  * make them independent, clear data, use data, nothing fishy
  * don't worry about async: if it affects global state, should be sync test anyway
[ ] figure out mnesia data directory better

## then
* tcp security, token system
* alerts on node failures, adjust routing
* release management
  * code change

## inglorious tasks remaining
* paging scheme for haps on auth / init user
  - maybe just top x for now
  - could get around by seriously shortening span...
* error handling
* SUB => make sure hap is not expired before sending
  [x] do it
  [ ] just need to write test (TODO)
* SUB => make sure hap user is not blocked before sending
  [x] do it
  [ ] just need to write test (TODO)
* validate username and email and secret
  [x] do it
  [ ] just need to write test (TODO)
* refactor duplicate checks

## eventually
* come up with better testing than timer for async
  - may need to spawn task, monitor, wait for death, then test?
  - or some sort of macro to send messages based on config when complete? listen in tests?
* index fo emails (in User > auth_tab)
* dry out haps saves in user -> repeated in sub and db... at least call same func!
* doc / spec out everythings

## testing
* bar must be in same mode as foo (env == :test) or else tables / nodes don't match!
```
  $ MIX_ENV=prod mix compile
  #Or on Windows:
  > set "MIX_ENV=prod" && mix compile
```

## more features
* address service (may just be front-end though)
* likes? flags?
  - maybe a general rep per user, rather than post, gets a +- 1
  - and on client, can remove posts, block users
* handling malicious users? ip?

## db maintenance
* db cleaner for expired haps
* deleted user / association cleaner, rather than at time of delete
  - consider spawning tasks for //ism, but risks hammering Mnesia
  - maybe pool somehow? or the above...
* archive or historic table?

## optimization
* use observer to monitor
  - try to find and alleviate any bottlenecks
* use dialyzer, specs, static code analysis?
* Tsung for load / performance testing when you have more / more powerful servers?
  * or, write your own simple one! put it on Hex!
  * specifically for tcp? then extend?
  * just spin up a bunch of processes (config, num or MAX) and nail it
  
## front end
* front-end app(s)!!!
* expand etf lib
